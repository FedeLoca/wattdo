package it.unimib.wattdo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import it.unimib.wattdo.model.User;
import it.unimib.wattdo.databinding.FragmentRegistrationBinding;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.AuthenticationViewModel;

/**
 * Fragment concerning the registration data entry
 */
public class RegistrationFragment extends Fragment {

    private static final String TAG = "RegistrationFragment";
    private FragmentRegistrationBinding binding;
    private FirebaseAuth myAuth;
    private FirebaseUser currentUser;
    private CallbackManager myCallbackManager;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private AuthenticationViewModel viewModel;

    private String username;
    private String country;
    private String birthDate;
    private String email;
    private String fullName;
    private String password;

    private boolean registrationComplete;
    private FirebaseRepository firebaseRepository;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        //code to generate the correct hash key to fix the facebook app bug but it does not work
        Log.d(TAG, "create");
        try {
            PackageInfo info = requireContext().getPackageManager().getPackageInfo("com.facebook.samples.hellofacebook", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e(TAG, e.toString());
        }
        */
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentRegistrationBinding.bind(view);
        myCallbackManager = CallbackManager.Factory.create();

        viewModel = new ViewModelProvider(requireActivity()).get(AuthenticationViewModel.class);
        firebaseRepository = new FirebaseRepository();

        //if the back button is pressed in the InterestsFragment coming back here, the registration is already completed and the user is
        //redirected to the LoginFragment
        if (registrationComplete) {
            Snackbar.make(binding.getRoot(), R.string.already_registered_snackbar, Snackbar.LENGTH_SHORT);

            firebaseRepository.facebookLogOut();
            firebaseRepository.firebaseLogOut();

            NavController navController = Navigation.findNavController(requireActivity(), R.id.authentication_nav_host_fragment);
            navController.navigate(R.id.loginFragment);
        }

        //the following instruction force to log with facebook only through web because there is a bug when the facebook app is installed
        //todo fixare con una soluzione definitiva il problema dell'app di facebook legato alla hash key
        binding.loginFacebookButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        //when the birth date text edit is clicked a dialog is shown
        binding.registrationBirthDateTextEdit.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            dateSetListener = (view1, year1, month1, dayOfMonth) -> {
                //when the date is inserted, it is set in the birth date text edit
                String dateString;

                //if (Locale.getDefault().equals(Locale.ITALY)) {
                    dateString = dayOfMonth + "/" + (month1 + 1) + "/" + year1;
                //}
                //else {
                //    dateString = year1 + "-" + (month1 + 1) + "-" + dayOfMonth;
                //}

                binding.registrationBirthDateTextEdit.setText(dateString);
            };

            //creates the dialog and shows it
            DatePickerDialog dialog = new DatePickerDialog(requireContext(), android.R.style.Theme_Material_Light_Dialog, dateSetListener, year, month, day);
            dialog.show();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.registrationSignUpButton.setOnClickListener(v -> {
        username = binding.registrationUsernameEditText.getText().toString();
        country = binding.registrationCountryTextEdit.getText().toString();
        birthDate = binding.registrationBirthDateTextEdit.getText().toString();
        email = binding.registrationEmailEditText.getText().toString();
        fullName = binding.registrationFullNameEditText.getText().toString();
        password = binding.registrationPasswordEditText.getText().toString();

        //when the sign up button is clicked the inserted data are checked
        if (checkRegistrationData()) {

            //buttons set unclickable to retain crash from multiple clicks
            binding.registrationSignUpButton.setClickable(false);
            binding.loginFacebookButton.setClickable(false);

            //check if the user email is already registered
            //String path = "/users/" + email.replace(".", "_DOT_");
            final LiveData<DataSnapshot> emailsLiveData = viewModel.getSingleDataSnapshotEmailsLiveData();
            emailsLiveData.observe(requireActivity(), dataSnapshot -> {

            if(dataSnapshot != null) {
                if (dataSnapshot.hasChild(email.replace(".", "_DOT_"))) {
                    binding.registrationSignUpButton.setClickable(true);
                    binding.loginFacebookButton.setClickable(true);

                    //if the email is already registered a message is displayed
                    Snackbar.make(binding.getRoot(), R.string.existing_email_snackbar, Snackbar.LENGTH_SHORT).show();
                } else {
                    myAuth = viewModel.getFirebaseAuth();

                    //if the email is not already registered the usernames list is read
                    final LiveData<DataSnapshot> usersLiveData1 = viewModel.getSingleDataSnapshotUsernamesLiveData();
                    usersLiveData1.observe(requireActivity(), dataSnapshot1 -> {

                        if(dataSnapshot1 != null){
                            if (dataSnapshot1.hasChild(username)) {
                                binding.registrationSignUpButton.setClickable(true);
                                binding.loginFacebookButton.setClickable(true);

                                //if the username already exists a message is displayed
                                Snackbar.make(binding.getRoot(), R.string.existing_username_snackbar, Snackbar.LENGTH_SHORT).show();
                            } else {

                                //if the username does not already exist the Firebase user is created
                                myAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {

                                        Log.d(TAG, "createUserWithEmail:success");

                                        //if it ends in a success, the user is created and put in the view model
                                        User user = new User(username, fullName, email, country, null, birthDate);

                                        viewModel.setCurrentUser(user);

                                        //the user data is added to the database
                                        firebaseRepository.setValue("users/" + email.replace(".", "_DOT_"), user);
                                        //the user username is added to the database
                                        firebaseRepository.setValue("usernames/" + username, email.replace(".", "_DOT_"));
                                        //the email is added to the database
                                        firebaseRepository.setValue("emails/" + email.replace(".", "_DOT_"), 0);

                                        currentUser = myAuth.getCurrentUser();
                                        if (currentUser != null)
                                            Log.d(TAG, currentUser.getUid());

                                        Snackbar.make(binding.getRoot(), R.string.registration_completed_snackbar, Snackbar.LENGTH_SHORT).show();

                                        registrationComplete = true;

                                        //the registration is completed and the InterestsFragment is shown specifying that it was called from the RegistrationFragment
                                        RegistrationFragmentDirections.SelectInterestsAction action = RegistrationFragmentDirections.selectInterestsAction("registration");
                                        NavController navController = Navigation.findNavController(requireView());
                                        navController.navigate(action);

                                        binding.registrationSignUpButton.setClickable(true);
                                        binding.loginFacebookButton.setClickable(true);
                                    } else {
                                        binding.registrationSignUpButton.setClickable(true);
                                        binding.loginFacebookButton.setClickable(true);

                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Snackbar.make(binding.getRoot(), R.string.registration_failed_snackbar + ": " + Objects.requireNonNull(task.getException()).getLocalizedMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        }
        else {
            binding.registrationSignUpButton.setClickable(true);
            binding.loginFacebookButton.setClickable(true);

            //if the inserted data didn't pass the controls a message is displayed
            }
        });

        //sets up facebook button
        binding.loginFacebookButton.setFragment(this);
        binding.loginFacebookButton.setPermissions("email", "public_profile");
        binding.loginFacebookButton.setOnClickListener(v -> {

        //buttons set unclickable to retain crash from multiple clicks
        binding.registrationSignUpButton.setClickable(false);
        binding.loginFacebookButton.setClickable(false);

        // manage login with Facebook
        binding.loginFacebookButton.registerCallback(myCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                Log.d(TAG, "facebook:onSuccess:" + loginResult);

                //if the facebook login ends in a success the FacebookRegistrationFragment is shown
                RegistrationFragmentDirections.FacebookRegistrationAction action = RegistrationFragmentDirections.facebookRegistrationAction(loginResult.getAccessToken().getToken());
                NavController navController = Navigation.findNavController(requireView());
                navController.navigate(action);

                binding.registrationSignUpButton.setClickable(true);
                binding.loginFacebookButton.setClickable(true);
            }

            @Override
            public void onCancel() {
                binding.registrationSignUpButton.setClickable(true);
                binding.loginFacebookButton.setClickable(true);
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                firebaseRepository.facebookLogOut();

                binding.registrationSignUpButton.setClickable(true);
                binding.loginFacebookButton.setClickable(true);

            }
        });
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        //if the view is destroyed logs out from Facebook
        firebaseRepository.facebookLogOut();
    }

    /**
     * Check the inserted data
     */
    private boolean checkRegistrationData() {
        return checkFullName() && checkEmail() && checkBirthDate() && checkCountry() && checkUsername() && checkPassword();
    }

    /**
     * Check the inserted full name
     */
    private boolean checkFullName() {
        if (fullName.length() >= 1 && fullName.length() <= 20)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.full_name_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted email
     */
    private boolean checkEmail() {
        if (checkInvalidEmailCharacters()) {
            Snackbar.make(binding.getRoot(), R.string.invalid_email_characters, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if (email.length() > 0)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.insert_email_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted country
     */
    private boolean checkCountry() {
        if (country.length() >= 3 && country.length() <= 20)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.country_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted birth date
     */
    private boolean checkBirthDate() {
        if (birthDate.length() > 0)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.insert_birthdate_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted username
     */
    private boolean checkUsername() {
        if (checkInvalidUsernameCharacters()) {
            Snackbar.make(binding.getRoot(), R.string.invalid_username_characters, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else if (username.length() >= 3 && username.length() <= 10)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.username_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check if the inserted email contains invalid characters
     */
    private boolean checkInvalidEmailCharacters() {
        return email.contains("#") || email.contains("$") || email.contains("[") || email.contains("]");
    }

    /**
     * Check if the inserted username contains invalid characters
     */
    private boolean checkInvalidUsernameCharacters() {
        return username.contains(".") || username.contains("#") || username.contains("$") || username.contains("[") || username.contains("]");
    }

    /**
     * Check the inserted password
     */
    private boolean checkPassword() {
        if (password.length() >= 8 && password.length() <= 20)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.password_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        myCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
