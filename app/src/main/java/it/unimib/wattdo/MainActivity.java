package it.unimib.wattdo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

import it.unimib.wattdo.databinding.ActivityMainBinding;

import com.facebook.login.LoginManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.MainViewModel;


/**
 * Activity that contains the fragments concerning the main features of the application.
 * It is the application launcher activity
 */
public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    private ActivityMainBinding binding;
    private MainActivity mainActivity = this;
    private User currentUser;
    private MainViewModel viewModel;

    private static final int REQUEST_LOCATION = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //obtain a new or prior instance of MainViewModel from the ViewModelProviders utility class.
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);

        //if the firebase user is null the AuthenticationActivity is launched
        FirebaseUser firebaseUser = viewModel.getFirebaseUser();
        if(firebaseUser == null) {
            startActivity(new Intent(this, AuthenticationActivity.class));
            this.finish();

        } else {

            //if the view model does not contain the current user
            if(viewModel.getCurrentUser() == null) {

                if(firebaseUser.getEmail() != null) {
                    String firebaseUserEmail = firebaseUser.getEmail().replace(".", "_DOT_");

                    Log.d(TAG, firebaseUserEmail);

                    //the user data are read from the database
                    String path = "/users/" + firebaseUserEmail;
                    final LiveData<DataSnapshot> usersLiveData = viewModel.getDataSnapshotPathLiveData(path);
                    usersLiveData.observe(this, dataSnapshot -> {

                        if(dataSnapshot != null) {
                            if (dataSnapshot.exists()) {

                                currentUser = dataSnapshot.getValue(User.class);

                                //a MutableLiveData containing the user is put in the view model
                                MutableLiveData<User> currentUserMutableLiveData = new MutableLiveData<>(currentUser);
                                viewModel.setCurrentUser(currentUserMutableLiveData);

                                //when the user data are retrieved check if the position permission was granted
                                gpsConnection();

                                //if the user data wasn't found on the database the user is logged out and the AuthenticationActivity is launched
                                if(currentUser == null) {
                                    //Snackbar.make(binding.getRoot(), "User do not exist", Snackbar.LENGTH_SHORT).show();

                                    LoginManager.getInstance().logOut();
                                    FirebaseAuth.getInstance().signOut();

                                    startActivity(new Intent(mainActivity, AuthenticationActivity.class));
                                    mainActivity.finish();
                                }

                                Log.d(TAG, currentUser.toString());

                            } else {

                                //if the user data wasn't found on the database the user is logged out and the AuthenticationActivity is launched

                                //Snackbar.make(binding.getRoot(), "User information do not exist in the database", Snackbar.LENGTH_SHORT).show();

                                LoginManager.getInstance().logOut();
                                FirebaseAuth.getInstance().signOut();

                                startActivity(new Intent(mainActivity, AuthenticationActivity.class));
                                mainActivity.finish();
                            }
                        }
                    });
                }
            } else {
                //if the view model already contains the current user it is retrieved from there
                currentUser = viewModel.getCurrentUser().getValue();

                //when the user data are retrieved check if the position permission was granted
                gpsConnection();
            }
        }
    }

    /**
     * Creates the activity view
     */
    public void createView() {

        if(binding == null){
            binding = ActivityMainBinding.inflate(getLayoutInflater());
            final View view = binding.getRoot();

            //set the starting fragment based on the corresponding intent extra value
            NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);  // Hostfragment
            NavGraph graph = null;
            if(navHostFragment != null){
                NavInflater inflater = navHostFragment.getNavController().getNavInflater();
                graph = inflater.inflate(R.navigation.nav_graph);
            }

            if(graph != null){
                if(getIntent() != null) {
                    String startFragment = getIntent().getStringExtra("fragment");
                    if(startFragment != null) {
                        switch (startFragment) {
                            case "profile":
                                graph.setStartDestination(R.id.profileFragment);
                                break;
                            case "groups":
                                graph.setStartDestination(R.id.groupsFragment);
                                break;
                            case "events":
                                graph.setStartDestination(R.id.myEventsFragment);
                                break;
                            default:
                                graph.setStartDestination(R.id.homeFragment);
                        }

                        getIntent().removeExtra("fragment");
                    }
                    else {
                        graph.setStartDestination(R.id.homeFragment);
                    }
                } else {
                    graph.setStartDestination(R.id.homeFragment);
                }
            }

            if(navHostFragment != null){
                navHostFragment.getNavController().setGraph(graph);
            }

            setContentView(view);

            final NavController navController = Navigation.findNavController(mainActivity, R.id.nav_host_fragment);
            NavigationUI.setupWithNavController(binding.bottomNavigation, navController);

            //when a navigation item is selected save a corresponding value in the view model and navigates to the corresponding fragment
            binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {

                switch (item.getItemId()) {
                    case R.id.profileFragment:
                        viewModel.setCurrentFragment("profile");
                        navController.navigate(item.getItemId());
                        break;
                    case R.id.groupsFragment:
                        viewModel.setCurrentFragment("groups");
                        navController.navigate(item.getItemId());
                        break;
                    case R.id.myEventsFragment:
                        viewModel.setCurrentFragment("events");
                        navController.navigate(item.getItemId());
                        break;
                    default:
                        viewModel.setCurrentFragment("home");
                        navController.navigate(item.getItemId());
                }

                return true;
            });

            //when the same navigation item is selected multiple times do nothing
            binding.bottomNavigation.setOnNavigationItemReselectedListener(item -> Log.d(TAG, "reselected"));

            //when the FAB is clicked launch the CreateEventActivity passing the current user
            binding.floatingActionButton.setOnClickListener(v -> {
                if(currentUser.getGroups() != null) {
                    if(currentUser.getGroups().size() > 0) {
                        Intent intent = new Intent(mainActivity, CreateEventActivity.class);
                        intent.putExtra("user", currentUser);
                        startActivity(intent);
                    } else{
                        Snackbar.make(binding.getRoot(), R.string.no_groups_snackbar, Snackbar.LENGTH_SHORT).show();
                    }
                }
                else{
                    Snackbar.make(binding.getRoot(), R.string.no_groups_snackbar, Snackbar.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_LOCATION){

            //if the permission was granted
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d(TAG, "granted");

                    //show the view
                    createView();

                } else {
                    //todo mostra un messaggio che dice che il permesso non è stato dato

                    //show the view
                    createView();
                }
            } else {

                //todo mostra un messaggio che dice che il permesso non è stato dato

                //show the view
                createView();
            }
        }
    }

    /**
     * Check if the position permission was already granted. If not the permission is asked
     */
    private void gpsConnection() {

        //if location permission is already granted
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

            Log.d(TAG, "permission already granted");

            //show the view
            createView();

        } else{

            //todo mostra un messaggio che dice spiega a cosa serve il permesso

            Log.d(TAG, "asking for permission");

            //ask for permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
    }
}
