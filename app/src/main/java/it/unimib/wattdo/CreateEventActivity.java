package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import it.unimib.wattdo.databinding.ActivityCreateEventBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.CreateEventViewModel;

/**
 * Activity that contains the fragments concerning the event creation process
 */
public class CreateEventActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCreateEventBinding binding = ActivityCreateEventBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        final CreateEventViewModel viewModel = new ViewModelProvider(this).get(CreateEventViewModel.class);

        //retrieve the current user from the intent and put it inside the view model
        Intent intent = getIntent();
        User currentUser = intent.getParcelableExtra("user");

        viewModel.setCurrentUser(currentUser);
    }
}
