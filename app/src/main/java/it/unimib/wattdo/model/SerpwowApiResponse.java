package it.unimib.wattdo.model;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * The SerpwowApiResponse class represent the response retrieved from the SerpWowApi
 */
public class SerpwowApiResponse {


    private RequestInfoSerpwow request_info;
    private List<Place> places_results;

    public SerpwowApiResponse(RequestInfoSerpwow request_info, List<Place> places_results) {
        this.request_info = request_info;
        this.places_results = places_results;
    }

    public RequestInfoSerpwow getRequest_info() {
        return request_info;
    }

    public void setRequest_info(RequestInfoSerpwow request_info) {
        this.request_info = request_info;
    }

    public List<Place> getPlaces_results() {
        return places_results;
    }

    public void setPlaces_results(List<Place> places_results) {
        this.places_results = places_results;
    }

    @NonNull
    @Override
    public String toString() {
        return "SerpwowApiResponse{" +
                "request_info=" + request_info +
                ", places_results=" + places_results +
                '}';
    }
}