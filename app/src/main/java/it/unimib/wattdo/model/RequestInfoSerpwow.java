package it.unimib.wattdo.model;

import androidx.annotation.NonNull;

/**
 * The RequestInfoSerpwow class represent objects that contains the result of a SerpWow HTTP response,
 * including the remaining credits for the free version
 */
public class RequestInfoSerpwow {
    private String success;
    private int credits_used;
    private int credits_remaining;

    public RequestInfoSerpwow(String success, int credits_used, int credits_remaining) {
        this.success = success;
        this.credits_used = credits_used;
        this.credits_remaining = credits_remaining;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public int getCredits_used() {
        return credits_used;
    }

    public void setCredits_used(int credits_used) {
        this.credits_used = credits_used;
    }

    public int getCredits_remaining() {
        return credits_remaining;
    }

    public void setCredits_remaining(int credits_remaining) {
        this.credits_remaining = credits_remaining;
    }

    @NonNull
    @Override
    public String toString() {
        return "RequestInfoSerpwow{" +
                "success='" + success + '\'' +
                ", credits_used=" + credits_used +
                ", credits_remaining=" + credits_remaining +
                '}';
    }
}
