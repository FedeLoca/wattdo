package it.unimib.wattdo.model;

import androidx.annotation.NonNull;

import java.util.Map;

/**
 * The Group class represent a group of users and contains all its data.
 */
public class Group {

    private String id;
    private String name;
    private Map<String, Integer> members;
    private String imageUrl;
    private Map<String, Integer> events;

    /**
     * Empty constructor needed to save and retrieve Group objects from the Firebase Real-Time Database
     */
    public Group() {
    }

    public Group(String id, String name, Map<String, Integer> members, String imageUrl, Map<String, Integer> events) {
        this.id = id;
        this.name = name;
        this.members = members;
        this.imageUrl = imageUrl;
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Integer> getMembers() {
        return members;
    }

    public void setMembers(Map<String, Integer> members) {
        this.members = members;
    }

    public Map<String, Integer> getEvents() {
        return events;
    }

    public void setEvents(Map<String, Integer> events) {
        this.events = events;
    }

    @NonNull
    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", members=" + members +
                ", imageUrl='" + imageUrl + '\'' +
                ", events=" + events +
                '}';
    }
}
