package it.unimib.wattdo.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * The Proposal class represent a proposal for an event and contains all its data.
 * It implements Parcelable in order to make possible to transfer it through intent extra.
 */
public class Proposal implements Parcelable {

    private String id;
    private String name;
    private String description;
    private String time;
    private String imageUrl;
    private String event;
    private Map<String, Integer> votes;

    /**
     * Empty constructor needed to retrieve Proposal objects from the Firebase Real-Time Database
     */
    public Proposal() {
    }

    public Proposal(String id, String name, String description, String time, String imageUrl, String event) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.time = time;
        this.imageUrl = imageUrl;
        this.event = event;
        this.votes = new HashMap<>();
    }

    protected Proposal(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        time = in.readString();
        imageUrl = in.readString();
        event = in.readString();
        votes = new HashMap<>();
        in.readMap(votes, Integer.class.getClassLoader());
    }

    public static final Creator<Proposal> CREATOR = new Creator<Proposal>() {
        @Override
        public Proposal createFromParcel(Parcel in) {
            return new Proposal(in);
        }

        @Override
        public Proposal[] newArray(int size) {
            return new Proposal[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Map<String, Integer> getVotes() {
        return votes;
    }

    public void setVotes(Map<String, Integer> votes) {
        this.votes = votes;
    }

    @NonNull
    @Override
    public String toString() {
        return "Proposal{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", time='" + time + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", event='" + event + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(time);
        dest.writeString(imageUrl);
        dest.writeString(event);
        dest.writeMap(votes);
    }
}
