package it.unimib.wattdo.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * The Place class represent a near place and contains all its data
 */
public class Place implements Parcelable {
    private String title;
    private String snippet;
    private String address;
    private String place_id;
    private String phone;

    public Place(String title, String snippet, String address, String place_id, String phone) {
        this.title = title;
        this.snippet = snippet;
        this.address = address;
        this.place_id = place_id;
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @NonNull
    @Override
    public String toString() {
        return "Place{" +
                "title='" + title + '\'' +
                ", snippet='" + snippet + '\'' +
                ", address='" + address + '\'' +
                ", place_id='" + place_id + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    //Parcelable

    protected Place(Parcel in) {
        title = in.readString();
        snippet = in.readString();
        address = in.readString();
        place_id = in.readString();
        phone = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(snippet);
        dest.writeString(address);
        dest.writeString(place_id);
        dest.writeString(phone);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
