package it.unimib.wattdo.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The Event class represent an event and contains all its data.
 * It implements Parcelable in order to make possible to transfer it through intent extra.
 */
public class Event implements Parcelable {

    private String id;
    private String eventName;
    private String description;
    private String date;
    private String fromTime;
    //private String toTime;
    private String pollEndTime;
    private Map<String, Proposal> proposals;
    private String group;
    private boolean pollEnded;
    private String pollEndTimeFormatted;
    private String result;
    private String groupName;

    /**
     * Empty constructor needed to retrieve Event objects from the Firebase Real-Time Database
     */
    public Event() {}

    public Event(String id, String eventName, String description, String date, String fromTime, String pollEndTime, String group) {
        this.id = id;
        this.eventName = eventName;
        this.description = description;
        this.date = date;
        this.fromTime = fromTime;
        //should be added in future
        //this.toTime = toTime;
        this.pollEndTime = pollEndTime;
        this.group = group;
        this.pollEnded = false;
        this.pollEndTimeFormatted = formatPollEndTime();
    }

    /**
     * Creates a string that represent the poll end's date and time in the ISO format used by the Joda library
     * yyyy-MM-dd'T'HH:mm
     */
    private String formatPollEndTime() {

        Date tmpDate = null;
        SimpleDateFormat input = new SimpleDateFormat("dd/MM/yyyy");
        try {
            tmpDate = input.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String convertedDateString = "";
        if (tmpDate != null) {
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
            convertedDateString = output.format(tmpDate);
        }

        return convertedDateString + 'T' + pollEndTime;
    }

    protected Event(Parcel in) {
        id = in.readString();
        eventName = in.readString();
        description = in.readString();
        date = in.readString();
        fromTime = in.readString();
        pollEndTime = in.readString();
        group = in.readString();
        proposals = new HashMap<>();
        in.readMap(proposals, Proposal.class.getClassLoader());
        pollEndTimeFormatted = in.readString();
        result = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    /*
    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
    */

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPollEndTime() {
        return pollEndTime;
    }

    public void setPollEndTime(String pollEndTime) {
        this.pollEndTime = pollEndTime;
    }

    public Map<String, Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(Map<String, Proposal> proposals) {
        this.proposals = proposals;
    }

    public boolean isPollEnded() {
        return pollEnded;
    }

    public void setPollEnded(boolean pollEnded) {
        this.pollEnded = pollEnded;
    }

    public String getPollEndTimeFormatted() {
        return pollEndTimeFormatted;
    }

    public void setPollEndTimeFormatted(String pollEndTimeFormatted) {
        this.pollEndTimeFormatted = pollEndTimeFormatted;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @NonNull
    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", eventName='" + eventName + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", fromTime='" + fromTime + '\'' +
                ", pollEndTime='" + pollEndTime + '\'' +
                ", proposals=" + proposals +
                ", group='" + group + '\'' +
                ", pollEnded=" + pollEnded +
                ", pollEndTimeFormatted='" + pollEndTimeFormatted + '\'' +
                ", result='" + result + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(eventName);
        dest.writeString(description);
        dest.writeString(date);
        dest.writeString(fromTime);
        dest.writeString(pollEndTime);
        dest.writeString(group);
        dest.writeMap(proposals);
        dest.writeString(pollEndTimeFormatted);
        dest.writeString(result);
    }
}
