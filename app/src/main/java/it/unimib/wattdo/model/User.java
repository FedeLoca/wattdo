package it.unimib.wattdo.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The User class represent a user and contains all its data.
 * It implements Parcelable in order to make possible to transfer it through intent extra.
 */
public class User implements Parcelable {

    private String username;
    private String fullName;
    private String email;
    private String country;
    private String birthDate;
    private int age;
    private Map<String, Integer> interests;
    private String imageUrl;
    private Map<String, Integer> groups;
    private Map<String, Integer> events;

    /**
     * Empty constructor needed to retrieve User objects from the Firebase Real-Time Database
     */
    public User() {
    }

    public User(String username, String fullName, String email, String country, String imageUrl, String birthDate) {
        this.username = username;
        this.fullName = fullName;
        this.imageUrl = imageUrl;
        this.email = email;
        this.country = country;
        this.birthDate = birthDate;
        this.interests = new HashMap<>();
        this.age = computeAge();
        this.groups = new HashMap<>();
    }

    protected User(Parcel in) {
        username = in.readString();
        fullName = in.readString();
        email = in.readString();
        country = in.readString();
        birthDate = in.readString();
        age = in.readInt();
        imageUrl = in.readString();
        interests = new HashMap<>();
        in.readMap(interests, Integer.class.getClassLoader());
        groups = new HashMap<>();
        in.readMap(groups, Integer.class.getClassLoader());
        events = new HashMap<>();
        in.readMap(events, Integer.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    /**
     * Computes the user age by calculating the difference from the current date and its birth date
     */
    private int computeAge() {

        if(birthDate != null) {
            String date;
            //if (Locale.getDefault().equals(Locale.ITALY)) {
                date = LocalDate.parse(birthDate, DateTimeFormat.forPattern("dd/MM/yyyy")).toString("yyyy-MM-dd");
            //}
            //else {
            //    date = LocalDate.parse(birthDate).toString();
            //}
            LocalDate convertedBirthDate = new LocalDate(date);;

            LocalDate currentDate = new LocalDate();

            Years age = Years.yearsBetween(convertedBirthDate, currentDate);

            return age.getYears();
        }
        else {
            return -1;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                ", username='" + username + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", country='" + country + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", age=" + age +
                ", interests=" + interests +
                ", imageUrl='" + imageUrl + '\'' +
                ", groups=" + groups +
                ", events=" + events +
                '}';
    }

    public String getEmailWithoutDot(){
        return email.replace(".", "_DOT_");
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public int getAge() {
        return age;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Map<String, Integer> getInterests() {
        return interests;
    }

    public void setInterests(Map<String, Integer> interests) {
        this.interests = interests;
    }

    public Map<String, Integer> getGroups() {
        return groups;
    }

    public void setGroups(Map<String, Integer> groups) {
        this.groups = groups;
    }

    public Map<String, Integer> getEvents() {
        return events;
    }

    public void setEvents(Map<String, Integer> events) {
        this.events = events;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(fullName);
        dest.writeString(email);
        dest.writeString(country);
        dest.writeString(birthDate);
        dest.writeInt(age);
        dest.writeString(imageUrl);
        dest.writeMap(interests);
        dest.writeMap(groups);
        dest.writeMap(events);
    }
}
