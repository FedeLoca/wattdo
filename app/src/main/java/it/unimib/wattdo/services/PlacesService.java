package it.unimib.wattdo.services;

import it.unimib.wattdo.model.SerpwowApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface that builds the query
 */
public interface PlacesService {

    @GET("search")
    Call<SerpwowApiResponse> getTopPlaces(@Query("api_key") String apiKey,
                                          @Query("q") String target,
                                          @Query("search_type") String type,
                                          @Query("location")String location,
                                          @Query("gl")String GoogleCountry,
                                          @Query("hl")String GoogleLanguage);
}
