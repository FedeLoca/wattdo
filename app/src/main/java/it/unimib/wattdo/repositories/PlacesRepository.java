package it.unimib.wattdo.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


import it.unimib.wattdo.model.Place;
import it.unimib.wattdo.model.Resource;
import it.unimib.wattdo.model.SerpwowApiResponse;
import it.unimib.wattdo.services.PlacesService;
import it.unimib.wattdo.utils.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Repository in which the places HTTP response is saved
 */
public class PlacesRepository {

    private static final String TAG = "PlacesRepository";

    private static PlacesRepository instance;
    private PlacesService placesService;

    private PlacesRepository(){

        //set read timeout to 15 seconds to receive API callback
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.PLACES_API_BASE_URL)
                .client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();

        placesService = retrofit.create(PlacesService.class);

    }

    public static synchronized PlacesRepository getInstance(){
        if (instance == null){
            instance = new PlacesRepository();
        }
        return instance;
    }

    /**
     * Makes the places HTTP request
     */
    public void getPlaces(MutableLiveData<Resource<List<Place>>> placesResource, String language, String country, double lat, double lon, String interest){

        String location = "lat:"+lat+",lon:"+lon;

        Call<SerpwowApiResponse> call = placesService.getTopPlaces(Constants.PLACES_API_KEY,interest,Constants.PLACES_API_TYPE,location, country.toLowerCase(), language);

        call.enqueue(new Callback<SerpwowApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<SerpwowApiResponse> call, @NonNull Response<SerpwowApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    Resource<List<Place>> resource = new Resource<>();
                    resource.setData(response.body().getPlaces_results());
                    resource.setStatusCode(response.code());
                    resource.setTotalResults(response.body().getPlaces_results().size());
                    resource.setStatusMessage(response.message());
                    placesResource.postValue(resource);

                    Log.d(TAG, "onResponse: " + response.body().getPlaces_results().size() + response.message());
                } else if (response.errorBody() != null){

                    Resource<List<Place>> resource = new Resource<>();

                    resource.setStatusCode(response.code());
                    try {
                        resource.setStatusMessage(response.errorBody().string() + "- "+ response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    placesResource.postValue(resource);

                }
            }
            @Override
            public void onFailure(@NonNull Call<SerpwowApiResponse> call, @NonNull Throwable t) {

                Resource<List<Place>> resource = new Resource<>();

                resource.setStatusMessage(t.getMessage());
                placesResource.postValue(resource);

                Log.d(TAG, "Error: " + t.getMessage());
            }
        });
    }
}
