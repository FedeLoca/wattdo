package it.unimib.wattdo.repositories;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * The FirebaseRepository class interfaces with the Firebase and Facebook services
 */
public class FirebaseRepository {

    //DatabaseReference of the main nodes of the Real-Time Database
    private final DatabaseReference USERS_REF = FirebaseDatabase.getInstance().getReference().child("users");
    private final DatabaseReference GROUPS_REF = FirebaseDatabase.getInstance().getReference().child("groups");
    private final DatabaseReference USERNAMES_REF = FirebaseDatabase.getInstance().getReference().child("usernames");
    private final DatabaseReference EVENTS_REF = FirebaseDatabase.getInstance().getReference().child("events");
    private final DatabaseReference EMAILS_REF = FirebaseDatabase.getInstance().getReference().child("emails");

    /**
     * Provides the current Firebase user
     */
    public FirebaseUser getFirebaseUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    /**
     * Provides the FirebaseAuth instance
     */
    public FirebaseAuth getFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    /**
     * Provides the DatabaseReference to a given path
     */
    public DatabaseReference getPATH_REF(String path) {
        return FirebaseDatabase.getInstance().getReference(path);
    }

    /**
     * Sets the given value to the given database path
     */
    public void setValue(String path, Object value) {
        FirebaseDatabase.getInstance().getReference(path).setValue(value);
    }

    /**
     * Remove the given value from the given database path
     */
    public void removeValue(String path) {
        FirebaseDatabase.getInstance().getReference(path).removeValue();
    }

    /**
     * Provides the id that a value would have if pushed at the given path
     */
    public String getPushId(String path){
        return FirebaseDatabase.getInstance().getReference(path).push().getKey();
    }

    /**
     * Logs out from the currently logged in Facebook account
     */
    public void facebookLogOut() {
        LoginManager lm = LoginManager.getInstance();
        if(lm != null) {
            lm.logOut();
        }
    }

    /**
     * Logs out from the currently logged in Firebase account
     */
    public void firebaseLogOut() {
        getFirebaseAuth().signOut();
    }

    public DatabaseReference getUSERS_REF() {
        return USERS_REF;
    }

    public DatabaseReference getGROUPS_REF() {
        return GROUPS_REF;
    }

    public DatabaseReference getUSERNAMES_REF() {
        return USERNAMES_REF;
    }

    public DatabaseReference getEVENTS_REF() {
        return  EVENTS_REF;
    }

    public DatabaseReference getEMAILS_REF() {
        return EMAILS_REF;
    }

}
