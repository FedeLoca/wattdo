package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import it.unimib.wattdo.databinding.ActivitySettingBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.SettingViewModel;

/**
 * Activity that contains the fragments concerning the profile settings changing process
 */
public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySettingBinding binding = ActivitySettingBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        final SettingViewModel viewModel = new ViewModelProvider(this).get(SettingViewModel.class);

        //retrieve the current user from the intent and put it inside the view model
        Intent intent = getIntent();
        User currentUser = intent.getParcelableExtra("user");

        viewModel.setCurrentUser(currentUser);
    }
}
