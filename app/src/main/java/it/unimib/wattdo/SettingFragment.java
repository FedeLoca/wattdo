package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;

import it.unimib.wattdo.databinding.FragmentSettingBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.SettingViewModel;

/**
 * Fragment concerning the user profile settings
 */
public class SettingFragment extends Fragment {

    private static final String TAG = "SettingFragment";

    private FragmentSettingBinding binding;
    private FirebaseRepository firebaseRepository;
    private User currentUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        firebaseRepository = new FirebaseRepository();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentSettingBinding.bind(view);

        //when the settings button is clicked launch the MainActivity in the ProfileFragment
        Button doneButton = binding.buttonSettingsDone;
        doneButton.setOnClickListener(v -> {
            Log.d(TAG, "done clicked");
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra("fragment", "profile");
            startActivity(intent);
            requireActivity().finish();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SettingViewModel viewModel = new ViewModelProvider(requireActivity()).get(SettingViewModel.class);
        currentUser = viewModel.getCurrentUser();
        Log.d(TAG, currentUser.toString());

        Button buttonSetPic = binding.buttonSetPic;
        if(binding.editTextSettingsPicUrl.length() > 0) {
            //when the set pic button is clicked set the user image to the inserted url in the database
            buttonSetPic.setOnClickListener(v -> {
                String url = binding.editTextSettingsPicUrl.getText().toString();
                String path = "users/" + currentUser.getEmailWithoutDot() + "/imageUrl";
                Snackbar.make(binding.getRoot(), R.string.pic_changed_snackbar, Snackbar.LENGTH_SHORT).show();
                firebaseRepository.setValue(path, url);
            });
        } else {
            Snackbar.make(binding.getRoot(), R.string.insert_url_snackbar, Snackbar.LENGTH_SHORT).show();
        }

        //when the change interests button is clicked show the InterestsFragment specifying that it was called to change the interests from the settings
        Button buttonSetInterests = binding.buttonSetInterests;
        buttonSetInterests.setOnClickListener(v -> {
            SettingFragmentDirections.ChangeInterestsAction action = SettingFragmentDirections.changeInterestsAction("setting");
            NavController navController = Navigation.findNavController(requireView());
            navController.navigate(action);
        });
    }
}
