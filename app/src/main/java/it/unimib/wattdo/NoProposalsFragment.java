package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import it.unimib.wattdo.databinding.FragmentNoProposalsBinding;

/**
 * Fragment concerning the no proposal screen
 */
public class NoProposalsFragment extends Fragment {

    private FragmentNoProposalsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentNoProposalsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //when the arrow button is clicked return to the main activity in the MyEventsFragment
        binding.imageButtonNoProposalsBack.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), MainActivity.class);
            intent.putExtra("fragment", "events");
            requireActivity().startActivity(intent);
            requireActivity().finish();
        });
    }
}
