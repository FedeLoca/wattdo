package it.unimib.wattdo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import it.unimib.wattdo.adapters.EventsAdapter;
import it.unimib.wattdo.databinding.FragmentMyEventsBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.MainViewModel;

/**
 * Fragment concerning the user events displaying
 */
public class MyEventsFragment extends Fragment {

    private final String TAG = "EventsFragment";

    private MainViewModel viewModel;
    private RecyclerView recyclerView;
    private User currentUser;
    private List<Event> eventList;

    private View viewTmp;
    private long counter = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        it.unimib.wattdo.databinding.FragmentMyEventsBinding binding = FragmentMyEventsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewTmp = view;

        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        //retrieve the current user from the view model
        currentUser = viewModel.getCurrentUser().getValue();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //sets up the recycler view
        recyclerView = viewTmp.findViewById(R.id.eventsRecyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //reads the user events list from the database
        String path = "users/" + currentUser.getEmailWithoutDot() + "/events";
        final LiveData<DataSnapshot> usersLiveData = viewModel.getDataSnapshotPathLiveData(path);
        usersLiveData.observe(getViewLifecycleOwner(), dataSnapshot -> {
            eventList = new ArrayList<>();

            if(dataSnapshot != null) {
                if (dataSnapshot.exists()) {

                    final long eventsNumber = dataSnapshot.getChildrenCount();

                    //iterates the events
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        final String dsKey = ds.getKey();

                        if (dsKey != null) {

                            //reads the event from the database
                            String path1 = "events/" + dsKey;
                            final LiveData<DataSnapshot> eventsLiveData = viewModel.getSingleDataSnapshotPathLiveData(path1);
                            eventsLiveData.observe(getViewLifecycleOwner(), dataSnapshot1 -> {

                                if(dataSnapshot1 != null){

                                    //adds the event to the eventList
                                    Event event = dataSnapshot1.getValue(Event.class);
                                    eventList.add(event);

                                    counter++;

                                    //if it is the last event
                                    if (counter == eventsNumber) {

                                        //create a MutableLiveData containing the event list and put it in the view model
                                        MutableLiveData<List<Event>> eventListMutableLiveData = new MutableLiveData<>(eventList);
                                        viewModel.setEventList(eventListMutableLiveData);

                                        //wait until the fragment is attached to the activity to create and set the recycler view adapter
                                        boolean adapterSet = false;
                                        while (!adapterSet) {
                                            if (isAdded()) {
                                                recyclerView.setAdapter(new EventsAdapter(currentUser, viewModel, ((MainActivity) requireActivity())));
                                                adapterSet = true;
                                            }
                                        }
                                        counter = 0;
                                    }
                                }
                            });
                        }
                    }
                } else {

                    //if the user does not have any events create a MutableLiveData containing the empty event list and put it in the view model
                    MutableLiveData<List<Event>> eventListMutableLiveData = new MutableLiveData<>(eventList);
                    viewModel.setEventList(eventListMutableLiveData);

                    //wait until the fragment is attached to the activity to create and set the recycler view adapter
                    boolean adapterSet = false;
                    while (!adapterSet) {
                        if (isAdded()) {
                            recyclerView.setAdapter(new EventsAdapter(currentUser, viewModel, ((MainActivity) requireActivity())));
                            adapterSet = true;
                        }
                    }
                }
            }
        });
    }
}
