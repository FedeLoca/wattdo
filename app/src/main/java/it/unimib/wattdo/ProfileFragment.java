package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import it.unimib.wattdo.adapters.InterestsAdapter;
import it.unimib.wattdo.databinding.FragmentProfileBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.MainViewModel;

/**
 * Fragment concerning the user profile
 */
public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private MainViewModel viewModel;
    private User currentUser;
    private FirebaseRepository firebaseRepository;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_profile, container, false);
        ImageButton btnOpenSetting = v.findViewById(R.id.imageSettingButton);

        //when the settings button is clicked launch the SettingActivity passing the current user
        btnOpenSetting.setOnClickListener(v1 -> {
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            intent.putExtra("user", currentUser);
            startActivity(intent);
        });

        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        firebaseRepository = new FirebaseRepository();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentProfileBinding.bind(view);

        //when the log out button is clicked, log out from facebook and firebase and launch the AuthenticationActivity
        binding.disconnectButton.setOnClickListener(v -> {
            firebaseRepository.facebookLogOut();
            firebaseRepository.firebaseLogOut();

            Intent intent = new Intent(requireActivity(), AuthenticationActivity.class);
            startActivity(intent);
            requireActivity().finish();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //when the current user info change update automatically their info
        viewModel.getCurrentUser().observe(requireActivity(), changedUser -> {
            currentUser = changedUser;

            TextView tv_user = binding.tvUser;
            tv_user.setText(currentUser.getUsername());

            TextView tv_country = binding.tvCountry;
            String countryAgeString = currentUser.getCountry() + ", " + currentUser.getAge();
            tv_country.setText(countryAgeString);

            TextView tv_fullName = binding.tvFullName;
            tv_fullName.setText(currentUser.getFullName());

            TextView tv_email = binding.tvEmail;
            tv_email.setText(currentUser.getEmail());

            TextView tv_birthDate = binding.tvBirthDate;
            tv_birthDate.setText(currentUser.getBirthDate());

            CircleImageView iw_pic = binding.iwPic;
            if(currentUser.getImageUrl() != null && !currentUser.getImageUrl().equals("")){
                Picasso.get().load(currentUser.getImageUrl()).into(iw_pic);
            }

            //make a list of interests from the map contained in the user object
            Map<String, Integer> interestsMap;
            if(currentUser.getInterests() != null){
                interestsMap = currentUser.getInterests();
            }
            else {
                interestsMap = new HashMap<>();
            }

            Iterator it = interestsMap.entrySet().iterator();

            List<String> interests = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                interests.add((String) entry.getKey());
            }

            //set up the RecyclerView also creating and setting  the adapter
            RecyclerView rv_interests = binding.rvInterests;
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            rv_interests.setLayoutManager(layoutManager);
            rv_interests.setAdapter(new InterestsAdapter(interests, getContext()));
        });
    }
}
