package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;

import it.unimib.wattdo.databinding.FragmentResultBinding;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.viewmodels.ResultViewModel;

/**
 * Fragment concerning the poll result showing
 */
public class ResultFragment extends Fragment {

    private final String TAG = "ResultFragment";

    private FragmentResultBinding binding;
    private Proposal proposalResult;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentResultBinding.inflate(getLayoutInflater());

        ResultViewModel viewModel = new ViewModelProvider(requireActivity()).get(ResultViewModel.class);
        proposalResult = viewModel.getProposalResult();

        if(proposalResult == null) {
            Log.d(TAG, "null");
        } else{
            Log.d(TAG, proposalResult.toString());
        }

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(proposalResult != null){
            setResultData(proposalResult);

            //when the arrow button is clicked return to the main activity in the MyEventsFragment
            binding.imageButtonResultBack.setOnClickListener(v -> {
                Intent intent = new Intent(requireActivity(), MainActivity.class);
                intent.putExtra("fragment", "events");
                requireActivity().startActivity(intent);
                requireActivity().finish();
            });
        }
    }



    /**
     * Sets the result card data using the give proposal's data
     */
    private void setResultData(Proposal proposal) {

        binding.textViewResultProposalNameValue.setText(proposal.getName());
        binding.textViewResultProposalTimeValue.setText(proposal.getTime());
        binding.textViewResultProposalDescriptionValue.setText(proposal.getDescription());

        if(proposal.getImageUrl() != null && !proposal.getImageUrl().equals("")) {
            Picasso.get().load(proposal.getImageUrl()).into(binding.imageViewResult);
        }
    }
}
