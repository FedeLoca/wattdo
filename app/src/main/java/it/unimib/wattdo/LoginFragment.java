package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import java.util.Objects;

import it.unimib.wattdo.databinding.FragmentLoginBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.AuthenticationViewModel;

/**
 * Fragment concerning the login data entry
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";
    private CallbackManager myCallbackManager;
    private FragmentLoginBinding binding;
    private FirebaseAuth myAuth;
    private AuthenticationViewModel viewModel;

    private String email;
    private String password;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myCallbackManager = CallbackManager.Factory.create();

        //the following instruction force to log with facebook only through web because there is a bug when the facebook app is installed
        //todo fixare con una soluzione definitiva il bug dell'app di facebook legato alla hash key
        binding.loginFacebookButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        viewModel = new ViewModelProvider(requireActivity()).get(AuthenticationViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.loginSignInButton.setOnClickListener(v -> {
            email = binding.loginEmailEditText.getText().toString();
            password = binding.loginPasswordEditText.getText().toString();

            //when the log in button is clicked the inserted credentials are checked
            if(checkEmail() && checkPassword()){
                Log.d(TAG, "signInButton clicked");

                //buttons set unclickable to retain crash from multiple clicks
                binding.loginSignInButton.setClickable(false);
                binding.loginFacebookButton.setClickable(false);
                binding.loginSignUpButton.setClickable(false);

                //sign in on firebase
                myAuth = viewModel.getFirebaseAuth();
                myAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");

                        Snackbar.make(binding.getRoot(), R.string.login_completed_snackbar, Snackbar.LENGTH_SHORT).show();

                        //retrieve user data from the database
                        String path = "users/" + email.replace(".", "_DOT_");
                        final LiveData<DataSnapshot> usersLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                        usersLiveData.observe(requireActivity(), dataSnapshot -> {

                            if(dataSnapshot != null) {
                                if (dataSnapshot.exists()) {

                                    User user = dataSnapshot.getValue(User.class);

                                    //when the user is created the MainActivity is launched passing the user
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.putExtra("user", user);
                                    startActivity(intent);
                                    requireActivity().finish();

                                    binding.loginSignInButton.setClickable(true);
                                    binding.loginFacebookButton.setClickable(true);
                                    binding.loginSignUpButton.setClickable(true);

                                } else {
                                    binding.loginSignInButton.setClickable(true);
                                    binding.loginFacebookButton.setClickable(true);
                                    binding.loginSignUpButton.setClickable(true);
                                }
                            }
                        });
                    } else {
                        binding.loginSignInButton.setClickable(true);
                        binding.loginFacebookButton.setClickable(true);
                        binding.loginSignUpButton.setClickable(true);

                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Snackbar.make(binding.getRoot(), R.string.invalid_credentials_snackbar, Snackbar.LENGTH_SHORT).show();
                    }
                });
            }
            else {
                //if the inserted credentials are not valid, display a message to the user
                Snackbar.make(binding.getRoot(), R.string.invalid_credentials_snackbar, Snackbar.LENGTH_SHORT).show();
            }
        });

        //when the sign up button is clicked the RegistrationFragment is shown
        binding.loginSignUpButton.setOnClickListener(v -> {
            Log.d(TAG, "signUpButton clicked");

            NavController navController = Navigation.findNavController(requireActivity(), R.id.authentication_nav_host_fragment);
            navController.navigate(R.id.registrationAction);
        });

        //sets up facebook button
        binding.loginFacebookButton.setFragment(this);
        binding.loginFacebookButton.setPermissions("email", "public_profile");
        binding.loginFacebookButton.setOnClickListener(v -> {
            // manage login with Facebook
            binding.loginFacebookButton.registerCallback(myCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d(TAG, "facebook:onSuccess:" + loginResult);

                    //buttons set unclickable to retain crash from multiple clicks
                    binding.loginSignInButton.setClickable(false);
                    binding.loginFacebookButton.setClickable(false);
                    binding.loginSignUpButton.setClickable(false);

                    //handle the Facebook token
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "facebook:onCancel");
                }

                @Override
                public void onError(FacebookException error) {
                    Log.d(TAG, "facebook:onError", error);
                }
            });
        });
    }

    /**
     * Handles the access token received from Facebook
     */
    private void handleFacebookAccessToken(final AccessToken accessToken) {
        //Facebook credentials
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());

        //sign in to firebase with the Facebbok credentials
        viewModel.getFirebaseAuth().signInWithCredential(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, "onComplete if: login to firebase");

                //if the log in is successful the firebase user is retrieved
                FirebaseUser currentUser = viewModel.getFirebaseUser();
                if (currentUser != null) {
                    final String currentUserEmail = Objects.requireNonNull(currentUser.getEmail()).replace(".", "_DOT_");

                    //checks if the firebase user email exists in the database
                    String path = "users/" + currentUserEmail;
                    final LiveData<DataSnapshot> usersLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                    usersLiveData.observe(requireActivity(), dataSnapshot -> {

                        if(dataSnapshot != null) {
                            if (dataSnapshot.exists()) {

                                //if the data exists in the database the user is created and the MainActivity is launched passing the user
                                User user = dataSnapshot.getValue(User.class);

                                Snackbar.make(binding.getRoot(), R.string.login_completed_snackbar, Snackbar.LENGTH_SHORT).show();

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra("user", user);
                                startActivity(intent);
                                requireActivity().finish();

                                binding.loginSignInButton.setClickable(true);
                                binding.loginFacebookButton.setClickable(true);
                                binding.loginSignUpButton.setClickable(true);

                            } else {

                                //if the user's email does not exist in the database logs out from firebase and shows the FacebookRegistrationFragment
                                FirebaseAuth.getInstance().signOut();

                                Snackbar.make(binding.getRoot(), R.string.register_first_snackbar, Snackbar.LENGTH_SHORT).show();

                                LoginFragmentDirections.FacebookRegistrationFragment2 action = LoginFragmentDirections.facebookRegistrationFragment2(accessToken.getToken());
                                NavController navController = Navigation.findNavController(requireView());
                                navController.navigate(action);

                                binding.loginSignInButton.setClickable(true);
                                binding.loginFacebookButton.setClickable(true);
                                binding.loginSignUpButton.setClickable(true);
                            }
                        }
                    });
                }
            }
            else {
                binding.loginSignInButton.setClickable(true);
                binding.loginFacebookButton.setClickable(true);
                binding.loginSignUpButton.setClickable(true);

                //if log in fails, display a message to the user
                Log.d(TAG, "onComplete else: could not login to firebase");
                Snackbar.make(binding.getRoot(), R.string.register_first_snackbar, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Check the inserted email
     */
    private boolean checkEmail() {
        return email.length() > 0;
    }

    /**
     * Check the inserted password
     */
    private boolean checkPassword() {
        return password.length() >= 8;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        myCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}

