package it.unimib.wattdo;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import java.util.Calendar;
import java.util.Locale;

import it.unimib.wattdo.databinding.FragmentFacebookRegistrationBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.AuthenticationViewModel;

/**
 * Fragment concerning the facebook registration data entry
 */
public class FacebookRegistrationFragment extends Fragment {

    private static final String TAG = "FBRegistrationFragment";
    private FragmentFacebookRegistrationBinding binding;
    private DatePickerDialog.OnDateSetListener dateSetListener;

    private String username;
    private String country;
    private String birthDate;

    private FirebaseUser firebaseUser;
    private String currentUserEmail;
    private String currentUserFullName;
    private boolean registrationComplete;
    private AuthenticationViewModel viewModel;
    private FirebaseRepository firebaseRepository;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_facebook_registration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding = FragmentFacebookRegistrationBinding.bind(view);

        viewModel = new ViewModelProvider(requireActivity()).get(AuthenticationViewModel.class);
        firebaseRepository = new FirebaseRepository();

        //if the back button is pressed in the InterestsFragment coming back here, the registration is already completed and the user is
        //redirected to the LoginFragment
        if (registrationComplete) {
            firebaseRepository.facebookLogOut();
            firebaseRepository.firebaseLogOut();

            Snackbar.make(binding.getRoot(), R.string.already_registered_snackbar, Snackbar.LENGTH_SHORT);
            NavController navController = Navigation.findNavController(requireActivity(), R.id.authentication_nav_host_fragment);
            navController.navigate(R.id.loginFragment);
        }

        //when the birth date text edit is clicked a dialog is shown
        binding.facebookRegistrationBirthDateTextEdit.setOnClickListener(v -> {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        dateSetListener = (view1, year1, month1, dayOfMonth) -> {

            //when the date is inserted, it is set in the birth date text edit
            String dateString;
            //if (Locale.getDefault().equals(Locale.ITALY)) {
                dateString = dayOfMonth + "/" + (month1 + 1)+ "/" + year1;
            //}
            //else {
            //    dateString = year1 + "-" + (month1 + 1) + "-" + dayOfMonth;
            //}
            binding.facebookRegistrationBirthDateTextEdit.setText(dateString);
        };

        //creates the dialog and shows it
        DatePickerDialog dialog = new DatePickerDialog(requireContext(), android.R.style.Theme_Material_Light_Dialog, dateSetListener, year, month, day);
        dialog.show();
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        //if the registration is not completed and the view is destroyed logs out from Facebook and Firebase
        if (!registrationComplete) {
            firebaseRepository.facebookLogOut();
            firebaseRepository.firebaseLogOut();
        }
    }

    /**
     * Handles the access token received from Facebook
     */
    private void handleFacebookAccessToken(final String accessToken) {
        //Facebook credentials
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken);

        //sign in to firebase with the Facebbok credentials
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(task -> {
        if (task.isSuccessful()) {
            Log.d(TAG, "onComplete: task successful");

            //if the log in is successful the users are retrieved from the database
            final LiveData<DataSnapshot> emailsLiveData = viewModel.getSingleDataSnapshotEmailsLiveData();
            emailsLiveData.observe(requireActivity(), dataSnapshot -> {

            if (dataSnapshot != null) {
                //the firebase user is retrieved
                firebaseUser = viewModel.getFirebaseUser();

                if (firebaseUser != null) {
                    currentUserEmail = firebaseUser.getEmail();
                    currentUserFullName = firebaseUser.getDisplayName();

                    if (currentUserEmail != null && currentUserFullName != null) {

                        //if a user with the Facebook email already exists go back to the LoginFragment
                        if (dataSnapshot.hasChild(currentUserEmail.replace(".", "_DOT_"))) {

                            Snackbar.make(binding.getRoot(), R.string.already_registered_snackbar, Snackbar.LENGTH_SHORT).show();

                            NavController navController = Navigation.findNavController(requireActivity(), R.id.authentication_nav_host_fragment);
                            navController.navigate(R.id.loginFragment);

                            binding.facebookRegistrationNextButton.setClickable(true);
                        }
                        else {

                            //if a user with the Facebook email does not already exist the usernames are retrieved from the database
                            final LiveData<DataSnapshot> usersLiveData1 = viewModel.getSingleDataSnapshotUsernamesLiveData();
                            usersLiveData1.observe(requireActivity(), dataSnapshot1 -> {

                                if (dataSnapshot1 != null) {
                                    if (dataSnapshot1.hasChild(username)) {

                                        //if a user with the inserted username already exists display a message
                                        binding.facebookRegistrationNextButton.setClickable(true);
                                        Snackbar.make(binding.getRoot(), R.string.existing_username_snackbar, Snackbar.LENGTH_SHORT).show();

                                    } else {

                                        //if a user with the inserted username does not already exists create the user and put it in the view model
                                        final User user = new User(username, currentUserFullName, currentUserEmail, country, null, birthDate);

                                        viewModel.setCurrentUser(user);

                                        //the user data is added to the database
                                        firebaseRepository.setValue("users/" + currentUserEmail.replace(".", "_DOT_"), user);
                                        //the user username is added to the database
                                        firebaseRepository.setValue("usernames/" + username, currentUserEmail.replace(".", "_DOT_"));
                                        //the email is added to the database
                                        firebaseRepository.setValue("emails/" + currentUserEmail.replace(".", "_DOT_"), 0);

                                        Snackbar.make(binding.getRoot(), R.string.registration_completed_snackbar, Snackbar.LENGTH_SHORT).show();

                                        registrationComplete = true;

                                        //the registration is completed and the InterestsFragment is shown
                                        FacebookRegistrationFragmentDirections.SelectInterestsAction action = FacebookRegistrationFragmentDirections.selectInterestsAction("registration");
                                        NavController navController = Navigation.findNavController(requireView());
                                        navController.navigate(action);

                                        binding.facebookRegistrationNextButton.setClickable(true);
                                    }

                                } else {
                                    binding.facebookRegistrationNextButton.setClickable(true);
                                }
                            });
                        }
                    } else {
                        binding.facebookRegistrationNextButton.setClickable(true);
                    }
                } else {
                    binding.facebookRegistrationNextButton.setClickable(true);
                }
            } else {
            binding.facebookRegistrationNextButton.setClickable(true);
            }
            });

        } else {
            binding.facebookRegistrationNextButton.setClickable(true);
            Log.d(TAG, "onComplete: task not successful" + task.getException());
        }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //when the next button is clicked check all inserted data
        binding.facebookRegistrationNextButton.setOnClickListener(v -> {
            username = binding.facebookRegistrationUsernameEditText.getText().toString();
            country = binding.facebookRegistrationCountryTextEdit.getText().toString();
            birthDate = binding.facebookRegistrationBirthDateTextEdit.getText().toString();

            if(checkUsername() && checkCountry() && checkBirthDate()) {
                Log.d(TAG, "facebookRegistrationNextButton: " + "buttonClicked");
                binding.facebookRegistrationNextButton.setClickable(false);

                String accessToken = FacebookRegistrationFragmentArgs.fromBundle(requireArguments()).getAccessToken();
                Log.d(TAG, accessToken);

                //if the inserted data passes the controls handle the Facebook token
                handleFacebookAccessToken(accessToken);
            }
        });
    }

    /**
     * Check the inserted country
     */
    private boolean checkCountry() {
        if (country.length() >= 3 && country.length() <= 20)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.country_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted birth date
     */
    private boolean checkBirthDate() {
        if (birthDate.length() > 0)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.insert_birthdate_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted username
     */
    private boolean checkUsername() {
        if (checkInvalidUsernameCharacters()) {
            Snackbar.make(binding.getRoot(), R.string.invalid_username_characters, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        if (username.length() >= 3 && username.length() <= 10)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.username_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check if the inserted username contains invalid characters
     */
    private boolean checkInvalidUsernameCharacters() {
        return username.contains(".") || username.contains("#") || username.contains("$") || username.contains("[") || username.contains("]");
    }

}
