package it.unimib.wattdo.viewmodels;

import it.unimib.wattdo.model.User;

/**
 * AuthenticationViewModel is the ViewModel that contains data used by the
 * AuthenticationActivity and its Fragments
 */
public class AuthenticationViewModel extends DatabaseViewModel {

    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
