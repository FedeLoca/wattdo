package it.unimib.wattdo.viewmodels;

import it.unimib.wattdo.model.User;

/**
 * SettingViewModel is the ViewModel that contains data used by the
 * SettingActivity and its Fragments
 */
public class SettingViewModel extends DatabaseViewModel {

    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
