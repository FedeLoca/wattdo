package it.unimib.wattdo.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Group;
import it.unimib.wattdo.model.Place;
import it.unimib.wattdo.model.Resource;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.PlacesRepository;

/**
 * MainViewModel is the ViewModel that contains data used by the
 * MainActivity and its Fragments
 */
public class MainViewModel extends DatabaseViewModel {

    private static final String TAG = "MainViewModel";

    private MutableLiveData<User> currentUser;
    private MutableLiveData<List<Group>> groupList;
    private MutableLiveData<List<Event>> eventList;
    private String currentFragment;

    public LiveData<Resource<List<Place>>> getPlacesResource(String interest, String language, String country, double lat, double lon){

        MutableLiveData<Resource<List<Place>>> places = new MutableLiveData<>();
        Log.d(TAG, "getPlaces: Download the places from internet");
        PlacesRepository.getInstance().getPlaces(places, language, country, lat, lon, interest);

        Log.d(TAG,"getPlaces: Places");
        return places;
    }

    public MutableLiveData<User> getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(MutableLiveData<User> currentUser) {
        this.currentUser = currentUser;
    }

    public MutableLiveData<List<Group>> getGroupList() {
        return groupList;
    }

    public void setGroupList(MutableLiveData<List<Group>> groupList) {
        this.groupList = groupList;
    }

    public MutableLiveData<List<Event>> getEventList() {
        return eventList;
    }

    public void setEventList(MutableLiveData<List<Event>> eventList) {
        this.eventList = eventList;
    }

    public String getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }
}
