package it.unimib.wattdo.viewmodels;

import java.util.List;

import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Group;
import it.unimib.wattdo.model.User;

/**
 * CreateEventViewModel is the ViewModel that contains data used by the
 * CreateEventActivity and its Fragments
 */
public class CreateEventViewModel extends DatabaseViewModel {

    private Event event;
    private User currentUser;
    private List<Group> groupList;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
