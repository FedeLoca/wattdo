package it.unimib.wattdo.viewmodels;

import java.util.List;

import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.model.User;

/**
 * PollViewModel is the ViewModel that contains data used by the
 * PollActivity and its Fragments
 */
public class PollViewModel extends DatabaseViewModel {

    private User currentUser;
    private Event event;
    private List<Proposal> proposalList;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public List<Proposal> getProposalList() {
        return proposalList;
    }

    public void setProposalList(List<Proposal> proposalList) {
        this.proposalList = proposalList;
    }
}
