package it.unimib.wattdo.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import it.unimib.wattdo.livedata.DatabaseLiveData;
import it.unimib.wattdo.repositories.FirebaseRepository;

/**
 * The DatabaseViewModel class is a ViewModel that contains all the DatabaseLiveData
 * of the main nodes of the Real-Time Database and can create new DatabaseLiveData using a given
 * path.
 * Is extended by all the ViewModels that read from the database
 */
public class DatabaseViewModel extends ViewModel {

    private FirebaseRepository firebaseRepository = new FirebaseRepository();

    //DatabaseLiveData of the main nodes of type single
    private final DatabaseLiveData usersSingleLiveData = new DatabaseLiveData(firebaseRepository.getUSERS_REF(), "single");
    private final DatabaseLiveData groupsSingleLiveData = new DatabaseLiveData(firebaseRepository.getGROUPS_REF(), "single");
    private final DatabaseLiveData usernamesSingleLiveData = new DatabaseLiveData(firebaseRepository.getUSERNAMES_REF(), "single");
    private final DatabaseLiveData emailsSingleLiveData = new DatabaseLiveData(firebaseRepository.getEMAILS_REF(), "single");

    //DatabaseLiveData of the main nodes of type multiple
    private final DatabaseLiveData usersLiveData = new DatabaseLiveData(firebaseRepository.getUSERS_REF(), "multiple");
    private final DatabaseLiveData groupsLiveData = new DatabaseLiveData(firebaseRepository.getGROUPS_REF(), "multiple");
    private final DatabaseLiveData usernamesLiveData = new DatabaseLiveData(firebaseRepository.getUSERNAMES_REF(), "multiple");
    private final DatabaseLiveData emailsLiveData = new DatabaseLiveData(firebaseRepository.getEMAILS_REF(), "multiple");


    /**
     * Creates new DatabaseLiveData of type single using a given path.
     */
    public LiveData<DataSnapshot> getSingleDataSnapshotPathLiveData(String path) {
        DatabaseReference pathRef = firebaseRepository.getPATH_REF(path);
        return new DatabaseLiveData(pathRef, "single");
    }

    /**
     * Creates new DatabaseLiveData of type multiple using a given path.
     */
    public LiveData<DataSnapshot> getDataSnapshotPathLiveData(String path) {
        DatabaseReference pathRef = firebaseRepository.getPATH_REF(path);
        return new DatabaseLiveData(pathRef, "multiple");
    }

    /**
     * Provides the current Firebase user
     */
    public FirebaseUser getFirebaseUser() {
        FirebaseUser firebaseUser = firebaseRepository.getFirebaseUser();
        return firebaseUser;
    }

    /**
     * Provides the FirebaseAuth instance
     */
    public FirebaseAuth getFirebaseAuth() {
        FirebaseAuth firebaseAuth = firebaseRepository.getFirebaseAuth();
        return firebaseAuth;
    }

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotUsersLiveData() {
        return usersLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotGroupsLiveData() {
        return groupsLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotUsernamesLiveData() {
        return usernamesLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotEmailsLiveData() {
        return emailsLiveData;
    }


    @NonNull
    public LiveData<DataSnapshot> getSingleDataSnapshotUsersLiveData() {
        return usersSingleLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getSingleDataSnapshotGroupsLiveData() {
        return groupsSingleLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getSingleDataSnapshotUsernamesLiveData() {
        return usernamesSingleLiveData;
    }

    @NonNull
    public LiveData<DataSnapshot> getSingleDataSnapshotEmailsLiveData() {
        return emailsSingleLiveData;
    }
}
