package it.unimib.wattdo.viewmodels;

import it.unimib.wattdo.model.Event;

/**
 * CreateProposalViewModel is the ViewModel that contains data used by the
 * CreateProposalActivity and its Fragments
 */
public class CreateProposalViewModel extends DatabaseViewModel{

    private Event event;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
