package it.unimib.wattdo.viewmodels;

import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Proposal;

/**
 * ResultViewModel is the ViewModel that contains data used by the
 * ResultActivity and its Fragments
 */
public class ResultViewModel extends DatabaseViewModel {

    private Event event;
    private Proposal proposalResult;

    public Proposal getProposalResult() {
        return proposalResult;
    }

    public void setProposalResult(Proposal proposalResult) {
        this.proposalResult = proposalResult;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
