package it.unimib.wattdo;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.unimib.wattdo.databinding.FragmentPlaceDetailBinding;
import it.unimib.wattdo.model.Place;


/**
 *
 */
public class PlaceDetailFragment extends Fragment {

    private static final String TAG = "PlaceDetailFragment";

    private FragmentPlaceDetailBinding binding;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public PlaceDetailFragment() {
        // Required empty public constructor
    }

    public static PlaceDetailFragment newInstance(String param1, String param2) {
        PlaceDetailFragment fragment = new PlaceDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPlaceDetailBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Place place = null;
        if(getArguments() != null){
            place = PlaceDetailFragmentArgs.fromBundle(getArguments()).getPlace();
        }

        if(place != null) {
            binding.tvPlaceTitle.setText(place.getTitle());
            binding.tvPlaceAddress.setText(place.getAddress());
            binding.tvPlacePhone.setText(place.getPhone());
            binding.tvPlaceSnippet.setText(place.getSnippet());

            Log.d(TAG,"Dettaglio: " + place);
        }
    }
}
