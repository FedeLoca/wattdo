package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;
import com.yuyakaido.android.cardstackview.SwipeableMethod;

import java.util.ArrayList;
import java.util.List;

import it.unimib.wattdo.adapters.ProposalsAdapter;
import it.unimib.wattdo.databinding.ActivityPollBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.PollViewModel;

/**
 * Activity concerning the poll vote process
 */
public class PollActivity extends AppCompatActivity {

    private final String TAG = "PollActivity";

    private CardStackView cardStackView;
    private CardStackLayoutManager layoutManager;
    private FirebaseRepository firebaseRepository;

    private Event event;
    private User currentUser;
    private List<Proposal> proposalList;

    private PollActivity activity = this;
    long counter = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityPollBinding binding = ActivityPollBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        final PollViewModel viewModel = new ViewModelProvider(this).get(PollViewModel.class);
        firebaseRepository = new FirebaseRepository();

        //retrieve the event and the current user from the intent and put them inside the view model
        Intent intent = getIntent();
        event = intent.getParcelableExtra("event");
        currentUser = intent.getParcelableExtra("user");

        viewModel.setEvent(event);
        viewModel.setCurrentUser(currentUser);

        cardStackView = view.findViewById(R.id.cardStackView);

        layoutManager = new CardStackLayoutManager(this, new CardStackListener() {
            @Override
            public void onCardDragging(Direction direction, float ratio) {
                Log.d(TAG, "dragging");
            }

            @Override
            public void onCardSwiped(Direction direction) {

                //retrieve the swiped proposal
                int position = layoutManager.getTopPosition();
                Proposal proposal = proposalList.get(position - 1);
                Log.d(TAG, proposal.toString());

                //if it was swiped right
                if(direction == Direction.Right) {
                    Log.d(TAG, "swiped right");

                    //if it wasn't already voted by the current user its vote is added
                    if(proposal.getVotes() != null) {

                        if(!proposal.getVotes().containsKey(currentUser.getEmailWithoutDot())) {
                            firebaseRepository.setValue("events/" + event.getId() + "/proposals/" + proposal.getId() + "/votes/" + currentUser.getEmailWithoutDot(), 0);
                        }
                    } else {
                        firebaseRepository.setValue("events/" + event.getId() + "/proposals/" + proposal.getId() + "/votes/" + currentUser.getEmailWithoutDot(), 0);
                    }
                }

                //if it was swiped left
                if(direction == Direction.Left) {
                    Log.d(TAG, "swiped left");

                    //if it was already voted by the current user its vote is removed
                    if(proposal.getVotes() != null) {
                        if(proposal.getVotes().containsKey(currentUser.getEmailWithoutDot())) {
                            firebaseRepository.removeValue("events/" + event.getId() + "/proposals/" + proposal.getId() + "/votes/" + currentUser.getEmailWithoutDot());
                        }
                    }
                }

                //if it was he last proposal returns to the MyEventsFragment in MainActivity
                if(position == proposalList.size()){
                    Intent intent = new Intent(activity, MainActivity.class);
                    intent.putExtra("fragment", "events");
                    activity.startActivity(intent);
                }
            }

            @Override
            public void onCardRewound() {
                Log.d(TAG, "rewound");
            }

            @Override
            public void onCardCanceled() {
                Log.d(TAG, "canceled");
            }

            @Override
            public void onCardAppeared(View view, int position) {
                Log.d(TAG, "appeared");
            }

            @Override
            public void onCardDisappeared(View view, int position) {
                Log.d(TAG, "disappeared");
            }
        });
        layoutManager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        layoutManager.setVisibleCount(3);
        layoutManager.setCanScrollVertical(false);
        layoutManager.setStackFrom(StackFrom.Top);
        layoutManager.setTranslationInterval(12.0f);
        cardStackView.setLayoutManager(layoutManager);

        FloatingActionButton rightFAB = binding.floatingActionButtonRight;
        FloatingActionButton leftFAB = binding.floatingActionButtonLeft;

        rightFAB.setOnClickListener(v -> {

            //swipe right
            SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder().setDirection(Direction.Right).build();
            layoutManager.setSwipeAnimationSetting(setting);
            cardStackView.swipe();
        });

        leftFAB.setOnClickListener(v -> {

            //swipe left
            SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder().setDirection(Direction.Left).build();
            layoutManager.setSwipeAnimationSetting(setting);
            cardStackView.swipe();
        });


        //reads the event proposals from the database
        String path = "events/" + event.getId() + "/proposals";
        final LiveData<DataSnapshot> proposalsLiveData = viewModel.getDataSnapshotPathLiveData(path);
        proposalsLiveData.observe(this, dataSnapshot -> {
            proposalList = new ArrayList<>();

            if(dataSnapshot != null) {
                if (dataSnapshot.exists()) {

                    final long proposalsNumber = dataSnapshot.getChildrenCount();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Proposal proposal = ds.getValue(Proposal.class);

                        if (proposal != null) {

                            proposalList.add(proposal);

                            counter++;

                            Log.d(TAG, proposal.toString());

                            //if it was the last proposal puts the proposalList in the view model and sets the card
                            //stack view adapter
                            if (counter == proposalsNumber) {

                                viewModel.setProposalList(proposalList);

                                cardStackView.setAdapter(new ProposalsAdapter(viewModel));
                                cardStackView.setItemAnimator(new DefaultItemAnimator());

                                counter = 0;
                            }
                        }
                    }
                } else {

                    //todo fare in modo che compaia che non ci sono proposte utilizzando magari il layout del no proposal fragment

                    //if the event has no proposals puts an empty proposalList in the view model and sets the card
                    //stack view adapter
                    viewModel.setProposalList(proposalList);

                    cardStackView.setAdapter(new ProposalsAdapter(viewModel));
                    cardStackView.setItemAnimator(new DefaultItemAnimator());
                }
            }
        });
    }
}
