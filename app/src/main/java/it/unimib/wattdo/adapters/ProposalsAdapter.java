package it.unimib.wattdo.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import it.unimib.wattdo.R;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.viewmodels.PollViewModel;

/**
 * Adapter for the poll CardStackView
 */
public class ProposalsAdapter extends RecyclerView.Adapter<ProposalsAdapter.ProposalsViewHolder> {

    private final String TAG = "ProposalsAdapter";

    //the list of proposals that have to be shown
    private List<Proposal> proposalList;

    public ProposalsAdapter(PollViewModel viewModel) {

        proposalList = viewModel.getProposalList();
    }

    @NonNull
    @Override
    public ProposalsAdapter.ProposalsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();

        View view = LayoutInflater.from(context).inflate(R.layout.proposal_item, viewGroup, false);
        return new ProposalsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProposalsAdapter.ProposalsViewHolder viewHolder, final int position) {

        viewHolder.bind(proposalList.get(position));
    }

    @Override
    public int getItemCount() {
        return proposalList.size();
    }

    public class ProposalsViewHolder extends RecyclerView.ViewHolder {

        private TextView proposalNameValueTextView;
        private TextView proposalTimeValueTextView;
        private TextView proposalDescriptionValueTextView;
        private ImageView proposalImageView;

        ProposalsViewHolder(View view) {
            super(view);

            proposalNameValueTextView = view.findViewById(R.id.textViewProposalNameValue);
            proposalTimeValueTextView = view.findViewById(R.id.textViewProposalTimeValue);
            proposalDescriptionValueTextView = view.findViewById(R.id.textViewProposalDescriptionValue);
            proposalImageView = view.findViewById(R.id.imageViewProposal);
        }

        public void bind(final Proposal proposal) {

            Log.d(TAG, proposal.getId());

            proposalNameValueTextView.setText(proposal.getName());
            proposalTimeValueTextView.setText(proposal.getTime());
            proposalDescriptionValueTextView.setText(proposal.getDescription());

            //sets the image view source to the image retrieved by Picasso at the specified url
            if(proposal.getImageUrl() != null && !proposal.getImageUrl().equals("")) {
                Picasso.get().load(proposal.getImageUrl()).into(proposalImageView);
            }
        }
    }

}
