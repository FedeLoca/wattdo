package it.unimib.wattdo.adapters;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import it.unimib.wattdo.CreateEventActivity;
import it.unimib.wattdo.MainActivity;
import it.unimib.wattdo.R;
import it.unimib.wattdo.databinding.FragmentGroupsBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Group;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.CreateEventViewModel;
import it.unimib.wattdo.viewmodels.DatabaseViewModel;
import it.unimib.wattdo.viewmodels.MainViewModel;

/**
 * Adapter for the RecyclerView that contains the user groups
 */
public class GroupsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "GroupsAdapter";

    private static final int GROUPS_VIEW_TYPE = 0;
    private static final int BUTTON_VIEW_TYPE = 1;
    private static final int TITLE_VIEW_TYPE = 2;

    private List<Group> groupList;
    private SparseBooleanArray expandState = new SparseBooleanArray();
    private Context context;
    private User currentUser;
    private FragmentGroupsBinding binding;
    private DatabaseViewModel viewModel;
    private LifecycleOwner activity;
    private FirebaseRepository firebaseRepository;
    private String caller;
    private Event event;

    private List<User> membersList = new ArrayList<>();
    private boolean allMembersExist = true;
    private int counter = -1;

    public GroupsAdapter(User currentUser, FragmentGroupsBinding binding, DatabaseViewModel viewModel, LifecycleOwner activity, String caller) {

        Log.d(TAG, "caller " + caller);

        if(caller.equals("event")) {

            //if the group fragment was called in a new event creation process retrieve the group list and the event from the right view model
            event = ((CreateEventViewModel) viewModel).getEvent();
            groupList = ((CreateEventViewModel) viewModel).getGroupList();
        } else {

            //if the group fragment was called in the main activity observe the MutableLiveData containing the group list from the right view model
            ((MainViewModel) viewModel).getGroupList().observe(activity, changedGroupList -> {

                //when the MutableLiveData change, the group list in this adapter is updated and the view is updated too
                groupList = changedGroupList;
                notifyDataSetChanged();
            });
        }

        //set initial expanded state to false
        for (int i = 0; i < groupList.size(); i++) {
            if(groupList.get(i) != null){
                Log.d(TAG, "group " + groupList.get(i).getName());
            }

            expandState.append(i, false);
        }

        this.currentUser = currentUser;
        this.binding = binding;
        this.viewModel = viewModel;
        this.activity = activity;
        this.caller = caller;
        firebaseRepository = new FirebaseRepository();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        this.context = viewGroup.getContext();

        //creates the right view holder based on the given view type
        if(viewType == GROUPS_VIEW_TYPE) {
            View view = LayoutInflater.from(context).inflate(R.layout.groups_item, viewGroup, false);
            return new GroupsViewHolder(view);
        } else if(viewType == BUTTON_VIEW_TYPE){
            View view = LayoutInflater.from(context).inflate(R.layout.add_button_item, viewGroup, false);
            return new ButtonViewHolder(view);
        } else{
            View view = LayoutInflater.from(context).inflate(R.layout.choose_group_title_item, viewGroup, false);
            return new TitleViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {

        //if the view holder is a GroupsViewHolder
        if(viewHolder instanceof  GroupsViewHolder){
            viewHolder.setIsRecyclable(false);

            //call the view holder bind method passing the group
            ((GroupsViewHolder) viewHolder).bind(groupList.get(position));

            //sets the view holder to not expanded
            ((GroupsViewHolder) viewHolder).expandableLayout.setVisibility(View.GONE);
            ((GroupsViewHolder) viewHolder).buttonLayout.setRotation(0f);
            ((GroupsViewHolder) viewHolder).buttonLayout.setOnClickListener(v -> onClickButton(((GroupsViewHolder) viewHolder).expandableLayout, ((GroupsViewHolder) viewHolder).buttonLayout,  position));

            //use this if you want to check if view is expanded and, if it is, keep it expanded. Cause problems with the inner recycler view
            //final boolean isExpanded = expandState.get(position);
            //((GroupsViewHolder) viewHolder).expandableLayout.setVisibility(isExpanded?View.VISIBLE:View.GONE);
            //((GroupsViewHolder) viewHolder).buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
            //((GroupsViewHolder) viewHolder).buttonLayout.setOnClickListener(v -> onClickButton(((GroupsViewHolder) viewHolder).expandableLayout, ((GroupsViewHolder) viewHolder).buttonLayout,  position));

        } else if (viewHolder instanceof  ButtonViewHolder){

            //if the view holder is a ButtonViewHolder call the view holder bind method passing the button listener
            ((ButtonViewHolder) viewHolder).bind(new CreateGroupButtonListener());
        } else {

            //if the view holder is a TitleViewHolder call the view holder bind method that do nothing
            ((TitleViewHolder) viewHolder).bind();
        }
    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    /**
     * CreateGroupButtonListener is the OnClickListener for the 'create a new group' button
     */
    private class CreateGroupButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            //an alert is created and shown
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            @SuppressLint("InflateParams") View createGroupPopup = LayoutInflater.from(context).inflate(R.layout.create_group_popup, null);

            final EditText groupNameEditText = createGroupPopup.findViewById(R.id.editTextGroupName);
            final EditText membersEditText = createGroupPopup.findViewById(R.id.editTextMembersList);
            final EditText groupUrlEditText = createGroupPopup.findViewById(R.id.editTextGroupUrl);

            //if the cancel button is pressed the dialog is closed and the group is not created
            alertBuilder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());

            //if the done button is pressed
            alertBuilder.setPositiveButton(R.string.done, (dialog, which) -> {

                //the inserted text of members usernames divided by comma is retrieved and spaces are eliminated
                String membersString = membersEditText.getText().toString();
                membersString = membersString.replace(" ", "");

                if (!checkInvalidUsernameCharacters(membersString)) {

                    final String groupNameString = groupNameEditText.getText().toString();

                    //the text is split using the comma as the separator and the usernames are put into a list
                    String[] membersStringArray = membersString.split(",");

                    // add members to list if the username is not empty
                    final List<String> membersStringList = new ArrayList<>();
                    for (String member : membersStringArray) {
                        if (!member.isEmpty())
                            membersStringList.add(member);
                    }

                    // add the current user as group member if not present
                    if (!membersStringList.contains(currentUser.getUsername()))
                        membersStringList.add(currentUser.getUsername());

                    //check if the group name was inserted and if group members are more than one
                    if (checkGroupName(groupNameString) && checkGroupMemberList(membersStringList)) {

                        //the current user is added to a list of User objects representing the group members
                        membersList.add(currentUser);

                        //the usernames list is iterated
                        for (int i = 0; i < membersStringList.size(); i++) {

                            String addedUserUsername = membersStringList.get(i);

                            Log.d(TAG, "username " + addedUserUsername);

                            //the existance of the username is checked reading from the database
                            String path = "/usernames/" + addedUserUsername;
                            final LiveData<DataSnapshot> usernamesLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                            usernamesLiveData.observe(activity, dataSnapshot -> {

                                if (dataSnapshot != null) {
                                    if (dataSnapshot.exists()) {

                                        //if the username exist, the corresponding email is read
                                        final String addedUserEmail = dataSnapshot.getValue(String.class);
                                        Log.d(TAG, "addedmail " + addedUserEmail);

                                        //the user corresponding to the email is read from the database
                                        String path1 = "/users/" + addedUserEmail;
                                        final LiveData<DataSnapshot> usersLiveData = viewModel.getSingleDataSnapshotPathLiveData(path1);
                                        usersLiveData.observe(activity, dataSnapshot1 -> {

                                            if (dataSnapshot1 != null) {

                                                User addedUser = dataSnapshot1.getValue(User.class);

                                                if (addedUser != null) {
                                                    Log.d(TAG, "added user " + addedUser.getEmail());
                                                }

                                                //the member's User object retrieved is added to the User objects list
                                                membersList.add(addedUser);

                                                counter++;

                                                //if all the member exist and have been added to the members User objects list
                                                if (allMembersExist && counter == membersStringList.size() - 1) {

                                                    //the members usernames list is transformed into a map
                                                    Map<String, Integer> membersUsernamesMap = new HashMap<>();
                                                    for (int j = 0; j < membersList.size(); j++) {
                                                        membersUsernamesMap.put(membersList.get(j).getUsername(), 0);
                                                    }

                                                    //remove comment to save only the members usernames in the database instead of the whole User objects
                                                /*
                                                Map<String, User> membersMap = new HashMap<>();
                                                for (int j = 0; j < membersList.size(); j++) {
                                                    membersMap.put(membersList.get(j).getUsername(), membersList.get(j));
                                                }

                                                Map<String, Integer> membersMap = new HashMap<>();
                                                for(int j = 0; j < membersList.size(); j++){
                                                    membersMap.put(membersList.get(j).getUsername(), 0);
                                                }
                                                */

                                                    //the group is created
                                                    Group newGroup = new Group("", groupNameString, membersUsernamesMap, groupUrlEditText.getText().toString(), null);

                                                    //the group's id is retrieved and set
                                                    String newGroupId = firebaseRepository.getPushId("groups");
                                                    newGroup.setId(newGroupId);

                                                    //the group is added to the database
                                                    firebaseRepository.setValue("groups/" + newGroupId, newGroup);

                                                    //the groups id is added to the member's groups list in the database
                                                    for (int j = 0; j < membersList.size(); j++) {
                                                        firebaseRepository.setValue("users/" + membersList.get(j).getEmailWithoutDot() + "/groups/" + newGroupId, 0);
                                                    }
                                                }
                                            }
                                        });
                                    } else {

                                        //if one or more usernames don't exist the boolean variable is changed and a message is displayed
                                        allMembersExist = false;
                                        Snackbar.make(binding.getRoot(), R.string.users_not_exist_snackbar, Snackbar.LENGTH_SHORT).show();
                                    }
                                }

                            });
                        }

                        //when the members list has been iterated the boolean variable is reset to the starting value
                        allMembersExist = true;
                    }
                }
            });

            //show the dialog
            alertBuilder.setView(createGroupPopup);
            alertBuilder.setTitle(R.string.create_a_new_group);
            final AlertDialog createGroupDialog = alertBuilder.create();

            createGroupDialog.setOnShowListener(arg0 -> {
                createGroupDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                createGroupDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            });

            createGroupDialog.show();
        }
    }

    /**
     * ViewHolder for a group item
     */
    public class GroupsViewHolder extends RecyclerView.ViewHolder{

        private TextView groupNameTextView;
        private RelativeLayout buttonLayout;
        private RelativeLayout expandableLayout;
        private RecyclerView membersRecyclerView;
        private CircleImageView imageViewGroup;
        private Button buttonLeaveGroup;
        private Button buttonGroupsCreateEvent;
        List<User> members;


        GroupsViewHolder(View view) {
            super(view);

            groupNameTextView = view.findViewById(R.id.textViewGroupName);
            membersRecyclerView = view.findViewById(R.id.membersRecyclerView);
            buttonLayout = view.findViewById(R.id.buttonExpand);
            expandableLayout = view.findViewById(R.id.relativeLayoutExpandable);
            imageViewGroup = view.findViewById(R.id.imageViewGroup);
            buttonLeaveGroup = view.findViewById(R.id.buttonLeaveGroup);
            buttonGroupsCreateEvent = view.findViewById(R.id.buttonGroupsCreateEvent);
        }

        public void bind(final Group group){

            //if the group fragment was not called in a new event creation process the create event button disappear
            if(!caller.equals("event")){
                buttonGroupsCreateEvent.setClickable(false);
                buttonGroupsCreateEvent.setVisibility(View.GONE);
            } else {

                //if the group fragment was called in a new event creation process the leave group button disappear
                buttonLeaveGroup.setClickable(false);
                buttonLeaveGroup.setVisibility(View.GONE);
            }

            groupNameTextView.setText(group.getName());

            if(group.getImageUrl() != null && !group.getImageUrl().equals("")){
                Picasso.get().load(group.getImageUrl()).into(imageViewGroup);
            }

            //when this group is chosen
            buttonGroupsCreateEvent.setOnClickListener(v -> {

                //the event group is set as the selected group id
                String groupId = group.getId();
                event.setGroup(groupId);

                //reads the name of the group to which the event belongs from the database and set it in the event object
                String path = "/groups/" + event.getGroup() + "/name";
                final LiveData<DataSnapshot> groupNameLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                groupNameLiveData.observe(activity, dataSnapshot -> {
                    if (dataSnapshot != null) {
                        if (dataSnapshot.exists()) {
                            String groupName = dataSnapshot.getValue(String.class);
                            event.setGroupName(groupName);

                            //the event id is retrieved from Firebase and set as the event id
                            final String newEventId = firebaseRepository.getPushId("events");
                            event.setId(newEventId);

                            //the event is added to the database
                            firebaseRepository.setValue("events/" + newEventId, event);
                            //the event id is added to the group's events list
                            firebaseRepository.setValue("groups/" + groupId + "/events/" + newEventId, 0);
                            //the event id is added to the current user's events list
                            firebaseRepository.setValue("users/" + currentUser.getEmailWithoutDot() + "/events/" + newEventId, 0);

                            //the groups members are retrieved and the current user is removed from it
                            Map<String, Integer> membersMap = group.getMembers();
                            membersMap.remove(currentUser.getUsername());

                            //the groups members are iterated
                            for (Map.Entry<String, Integer> stringIntegerEntry : membersMap.entrySet()) {

                                Log.d(TAG, ((Map.Entry) stringIntegerEntry).getKey().toString());

                                //the email corresponding to the member username is read
                                String path1 = "/usernames/" + ((Map.Entry) stringIntegerEntry).getKey();
                                final LiveData<DataSnapshot> usernamesLiveData = viewModel.getSingleDataSnapshotPathLiveData(path1);
                                usernamesLiveData.observe(activity, dataSnapshot1 -> {

                                    if (dataSnapshot1 != null) {
                                        if (dataSnapshot1.exists()) {
                                            final String memberEmail = dataSnapshot1.getValue(String.class);

                                            if(memberEmail != null){
                                                Log.d(TAG, memberEmail);
                                            }

                                            //the event id is added to the member's events list
                                            firebaseRepository.setValue("users/" + memberEmail + "/events/" + newEventId, 0);

                                            //the caller is set to main (as it is the default value) before launching the MainActivity in the MyEventsFragment
                                            caller = "main";

                                            Intent intent = new Intent(((CreateEventActivity) activity), MainActivity.class);
                                            intent.putExtra("fragment", "events");
                                            context.startActivity(intent);
                                            ((CreateEventActivity) activity).finish();
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            });

            //when the leave button of this group is clicked
            buttonLeaveGroup.setOnClickListener(v -> {

                //the groups members are retrieved and the current user is removed from it
                Map<String, Integer> groupMembers = group.getMembers();
                groupMembers.remove(currentUser.getUsername());

                //if the current user was the last group member
                if(groupMembers.size() == 0){

                    //the group is deleted from the database
                    firebaseRepository.removeValue("groups/" + group.getId());

                    //the group's events are removed from the database and from the current user's events in the database:
                    //the group's events are retrieved and iterated
                    Map<String, Integer> eventsMap = group.getEvents();
                    if(eventsMap != null) {

                        for (Map.Entry<String, Integer> stringIntegerEntry : eventsMap.entrySet()) {

                            //the event is removed from the database
                            firebaseRepository.removeValue("events/" + ((Map.Entry) stringIntegerEntry).getKey());
                            //the event is removed from the current user's events in the database
                            firebaseRepository.removeValue("users/" + currentUser.getEmailWithoutDot() + "/events/" + ((Map.Entry) stringIntegerEntry).getKey());
                        }
                    }
                }
                else {

                    //if the current user wasn't the last group member the current user is removed from the group's members in the database
                    firebaseRepository.removeValue("groups/" + group.getId() + "/members/" + currentUser.getUsername());

                    //the group's events are removed from the current user's events:
                    //the group's events are retrieved and iterated
                    Map<String, Integer> eventsMap = group.getEvents();
                    if(eventsMap != null) {

                        for (Map.Entry<String, Integer> stringIntegerEntry : eventsMap.entrySet()) {

                            //the event is removed from the current user's events in the database
                            firebaseRepository.removeValue("users/" + currentUser.getEmailWithoutDot() + "/events/" + ((Map.Entry) stringIntegerEntry).getKey());
                        }
                    }
                }

                //the group is removed from the current user's groups in the database
                firebaseRepository.removeValue("users/" + currentUser.getEmailWithoutDot() + "/groups/" + group.getId());
            });

            //the groups members are retrieved and iterated
            Map<String, Integer> membersMap = group.getMembers();

            Iterator it = membersMap.entrySet().iterator();
            members = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();

                //the members email corresponding to the username is read from the database
                String path = "/usernames/" + entry.getKey();
                final LiveData<DataSnapshot> usernamesLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                usernamesLiveData.observe(activity, dataSnapshot -> {

                    if(dataSnapshot != null) {
                        if (dataSnapshot.exists()) {

                            final String memberEmail = dataSnapshot.getValue(String.class);

                            //the member data are retrieved using their email and added to a list
                            String path1 = "/users/" + memberEmail;
                            final LiveData<DataSnapshot> usersLiveData = viewModel.getSingleDataSnapshotPathLiveData(path1);
                            usersLiveData.observe(activity, dataSnapshot1 -> {
                                if(dataSnapshot1 != null) {
                                    if (dataSnapshot1.exists()) {
                                        User member = dataSnapshot1.getValue(User.class);

                                        members.add(member);
                                    }
                                }
                            });
                        }
                    }
                });
            }

            //the recycler view nested in the group element is set up
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            membersRecyclerView.setLayoutManager(layoutManager);

            //the adapter is created using the member list is created and set as the nested recycler view adapter
            membersRecyclerView.setAdapter(new MembersAdapter(members));
        }
    }

    /**
     * ViewHolder for a "create new group" button
     */
    public static class ButtonViewHolder extends RecyclerView.ViewHolder {

        Button buttonCreateGroup;

        ButtonViewHolder(View view){
            super(view);
            buttonCreateGroup = view.findViewById(R.id.buttonCreateGroup);
        }

        public void bind(View.OnClickListener onClickListener) {
            buttonCreateGroup.setOnClickListener(onClickListener);
        }
    }

    /**
     * ViewHolder for a "choose a group" title
     */
    public static class TitleViewHolder extends RecyclerView.ViewHolder {

        TitleViewHolder(View view){
            super(view);
        }

        public void bind() { }
    }

    /**
     * Expand the ViewHolder when the expand button is clicked and collapse it when its clicked again
     */
    private void onClickButton(final RelativeLayout expandableLayout, final RelativeLayout buttonLayout, final  int i) {

        //set View to Gone if not expanded
        if (expandableLayout.getVisibility() == View.VISIBLE){
            createRotateAnimator(buttonLayout, 180f, 0f).start();
            expandableLayout.setVisibility(View.GONE);
            expandState.put(i, false);
        }else{
            createRotateAnimator(buttonLayout, 0f, 180f).start();
            expandableLayout.setVisibility(View.VISIBLE);
            expandState.put(i, true);
        }
    }

    /**
     * Rotate the expand button
     */
    private ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    /**
     * Returns the view type for an element of the group list
     */
    public int getItemViewType(int position) {

        //if the element is null
        if(groupList.get(position) == null) {

            //if the group fragment was called in a new event creation process
            if(caller.equals("event")) {

                //the element is the "choose a group" title
                return TITLE_VIEW_TYPE;
            }
            else {

                //otherwise the element is the "create a new group" button
                return BUTTON_VIEW_TYPE;
            }
        } else {

            //if the element is null the element is a group item
            return GROUPS_VIEW_TYPE;
        }
    }

    /**
     * Check the inserted group name
     */
    private boolean checkGroupName(String groupName) {
        if (groupName.length() > 0 && groupName.length() <= 10)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.group_name_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the group members list size
     */
    private boolean checkGroupMemberList(List<String> membersList) {
        if (membersList.size() > 1)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.group_member_list_size_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check if the inserted members' username contains invalid characters
     */
    private boolean checkInvalidUsernameCharacters(String usernameMembers) {
        if (usernameMembers.contains(".") || usernameMembers.contains("#") || usernameMembers.contains("$") || usernameMembers.contains("[") || usernameMembers.contains("]")) {
            Snackbar.make(binding.getRoot(), R.string.invalid_username_characters, Snackbar.LENGTH_SHORT).show();
            return true;
        }
        else
            return false;
    }
}
