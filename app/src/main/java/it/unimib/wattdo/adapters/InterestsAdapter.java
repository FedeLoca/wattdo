package it.unimib.wattdo.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.unimib.wattdo.R;

/**
 * Adapter for the profile interests RecyclerView
 */
public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.InterestsViewHolder>{

    private final String TAG = "InterestsAdapter";

    //the list of interests that have to be shown
    private List<String> interests;
    private Context context;

    public InterestsAdapter(List<String> interests, Context context) {
        this.interests = interests;
        this.context = context;
    }

    @NonNull
    @Override
    public InterestsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interests_item, parent, false);
        return new InterestsAdapter.InterestsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterestsViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        holder.bind(interests.get(position));
    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public class InterestsViewHolder extends RecyclerView.ViewHolder{

        private TextView interestNameTextView;

        InterestsViewHolder(View view) {
            super(view);

            interestNameTextView = view.findViewById(R.id.interestNameTextView);
        }

        public void bind(String interest){

            Resources res = context.getResources();
            String interestString = "";

            switch (interest){
                case "bar":
                    interestString = res.getString(R.string.bar);
                    break;

                case "games":
                    interestString = res.getString(R.string.games);
                    break;

                case "cinema":
                    interestString = res.getString(R.string.cinema);
                    break;

                case "restaurant":
                    interestString = res.getString(R.string.restaurant);
                    break;

                case "sport":
                    interestString = res.getString(R.string.sport);
                    break;

                case "theatre":
                    interestString = res.getString(R.string.theatre);
                    break;

                default:
                    break;
            }

            Log.d(TAG, interest);
            interestNameTextView.setText(interestString);
        }
    }
}
