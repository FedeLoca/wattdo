package it.unimib.wattdo.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;

import org.joda.time.LocalDateTime;

import java.util.List;

import it.unimib.wattdo.CreateProposalActivity;
import it.unimib.wattdo.MainActivity;
import it.unimib.wattdo.PollActivity;
import it.unimib.wattdo.R;
import it.unimib.wattdo.ResultActivity;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.MainViewModel;

/**
 * Adapter for the RecyclerView that contains the user events
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private final String TAG = "EventsAdapter";

    private List<Event> eventList;
    private SparseBooleanArray expandState = new SparseBooleanArray();
    private Context context;
    private User currentUser;
    private MainViewModel viewModel;
    private MainActivity activity;
    private FirebaseRepository firebaseRepository;

    public EventsAdapter(User currentUser, MainViewModel viewModel, MainActivity activity) {

        //observe the MutableLiveData in the view model that contains the list of events
        viewModel.getEventList().observe(activity, changedEventList -> {

            //when the MutableLiveData change, the event list in this adapter is updated and the view is updated too
            eventList = changedEventList;
            notifyDataSetChanged();
        });

        //set initial expanded state to false
        for (int i = 0; i < eventList.size(); i++) {
            if(eventList.get(i) != null){
                Log.d(TAG, "event " + eventList.get(i).getEventName());
            }

            expandState.append(i, false);
        }

        this.currentUser = currentUser;
        this.viewModel = viewModel;
        this.activity = activity;
        firebaseRepository = new FirebaseRepository();
    }

    @NonNull
    @Override
    public EventsAdapter.EventsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        this.context = viewGroup.getContext();

        View view = LayoutInflater.from(context).inflate(R.layout.events_item, viewGroup, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EventsAdapter.EventsViewHolder viewHolder, final int position) {

        viewHolder.setIsRecyclable(false);

        viewHolder.bind(eventList.get(position));

        //sets the view holder to not expanded
        viewHolder.expandableLayout.setVisibility(View.GONE);
        viewHolder.buttonLayout.setRotation(0f);
        viewHolder.buttonLayout.setOnClickListener(v -> onClickButton(viewHolder.expandableLayout, viewHolder.buttonLayout,  position));

        //use this if you want to check if view is expanded and, if it is, keep it expanded. Cause problems with the inner recycler view
        //final boolean isExpanded = expandState.get(position);
        //viewHolder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        //viewHolder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        //viewHolder.buttonLayout.setOnClickListener(v -> onClickButton(viewHolder.expandableLayout, viewHolder.buttonLayout, position));
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    /**
     * ViewHolder for an event item
     */
    public class EventsViewHolder extends RecyclerView.ViewHolder {

        private TextView eventNameTextView;
        private TextView eventGroupNameTextView;
        private TextView eventPollEndValue;
        private TextView eventDateValue;
        private TextView eventFromTimeValue;
        private TextView eventDescriptionValue;
        private RelativeLayout buttonLayout;
        private LinearLayout expandableLayout;
        private Button buttonEventsVote;
        private Button buttonCreateProposal;


        EventsViewHolder(View view) {
            super(view);

            eventNameTextView = view.findViewById(R.id.textViewEventName);
            eventGroupNameTextView = view.findViewById(R.id.textViewEventGroupName);
            eventPollEndValue = view.findViewById(R.id.textViewEventPollEndValue);
            eventDateValue = view.findViewById(R.id.textViewEventDateValue);
            eventFromTimeValue = view.findViewById(R.id.textViewEventFromTimeValue);
            eventDescriptionValue = view.findViewById(R.id.textViewEventDescriptionValue);
            buttonLayout = view.findViewById(R.id.buttonEventExpand);
            expandableLayout = view.findViewById(R.id.linearLayoutExpandable);
            buttonCreateProposal = view.findViewById(R.id.buttonCreateProposal);
            buttonEventsVote = view.findViewById(R.id.buttonEventsVote);
        }

        public void bind(final Event event) {

            eventNameTextView.setText(event.getEventName());
            eventPollEndValue.setText(event.getPollEndTime());
            eventDateValue.setText(event.getDate());
            eventFromTimeValue.setText(event.getFromTime());
            eventDescriptionValue.setText(event.getDescription());
            eventGroupNameTextView.setText(event.getGroupName());

            //if the event's poll is declared as ended
            if(event.isPollEnded()) {

                //the UI is set as ended
                setEndedUI(event);
            } else {

                //if the event's poll is not declared as ended checks if it is ended by comparing the poll end time with the current time
                LocalDateTime currentDateTime = new LocalDateTime();
                LocalDateTime pollEndDateTime = new LocalDateTime(event.getPollEndTimeFormatted());
                if (currentDateTime.isAfter(pollEndDateTime)) {

                    //if the poll end time is passed the event's poll is declared as ended in the database
                    firebaseRepository.setValue("events/" + event.getId() + "/pollEnded", true);

                    //the UI is set as ended
                    setEndedUI(event);
                } else {

                    //if the event's poll is not ended yet
                    //when the create proposal button is clicked the CreateProposalActivity is launched passing the event
                    buttonCreateProposal.setOnClickListener(v -> {
                        Intent intent = new Intent(activity, CreateProposalActivity.class);
                        intent.putExtra("event", event);
                        context.startActivity(intent);
                    });

                    //when the vote button is clicked the PollActivity is launched passing the event and the current user
                    buttonEventsVote.setOnClickListener(v -> {
                        Intent intent = new Intent(activity, PollActivity.class);
                        intent.putExtra("event", event);
                        intent.putExtra("user", currentUser);
                        context.startActivity(intent);
                    });
                }
            }
        }

        /**
         * Sets the ViewHolder UI as the event's poll is ended
         */
        private void setEndedUI(final Event event) {

            //the button to create new proposals disappear
            buttonCreateProposal.setVisibility(View.GONE);
            buttonCreateProposal.setClickable(false);

            //the button to vote change its text and set a listener. When it's clicked the ResultActivity is launched
            buttonEventsVote.setText(R.string.result);

            buttonEventsVote.setOnClickListener(v -> {
                Intent intent = new Intent(activity, ResultActivity.class);
                intent.putExtra("event", event);
                context.startActivity(intent);
            });
        }
    }

    /**
     * Expand the ViewHolder when the expand button is clicked and collapse it when its clicked again
     */
    private void onClickButton(final LinearLayout expandableLayout, final RelativeLayout buttonLayout, final  int i) {

        //set View to Gone if not expanded
        if (expandableLayout.getVisibility() == View.VISIBLE){
            createRotateAnimator(buttonLayout, 180f, 0f).start();
            expandableLayout.setVisibility(View.GONE);
            expandState.put(i, false);
        }else{
            createRotateAnimator(buttonLayout, 0f, 180f).start();
            expandableLayout.setVisibility(View.VISIBLE);
            expandState.put(i, true);
        }
    }

    /**
     * Rotate the expand button
     */
    private ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }
}
