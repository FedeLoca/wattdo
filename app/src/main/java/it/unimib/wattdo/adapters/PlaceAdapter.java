package it.unimib.wattdo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.unimib.wattdo.R;
import it.unimib.wattdo.model.Place;

/**
 * Adapter for the places RecyclerView
 */
public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlacesViewHolder> {

    private final String TAG = "HomeAdapter";

    private List<Place> placeList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {

        TextView tv_PlaceTitle;
        TextView tv_PlaceAddress;

        PlacesViewHolder(View view){
            super(view);
            tv_PlaceTitle = view.findViewById(R.id.tv_PlaceTitle);
            tv_PlaceAddress = view.findViewById(R.id.tv_PlaceAddress);
        }

        public void bind(Place place, OnItemClickListener onItemClickListener){
            tv_PlaceTitle.setText(place.getTitle());
            tv_PlaceAddress.setText(place.getAddress());

            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(place));
        }
    }

    public PlaceAdapter(Context context, List<Place> placeList, OnItemClickListener onItemClickListener){
        this.layoutInflater = LayoutInflater.from(context);
        this.placeList = placeList;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(Place place);
    }

    @NonNull
    @Override
    public PlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.place_item, parent,false);
        return new PlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewHolder holder, int position) {
        holder.bind(placeList.get(position), this.onItemClickListener);
        //holder.tv_PlaceTitle.setText(placeList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {

        if(placeList != null){
            return placeList.size();
        }

        return 0;
    }

    /*private static final int POST_VIEW_TYPE = 0;
    private static final int LOADING_VIEW_TYPE = 1;

    public interface OnItemClickListener{
        void onItemClick(Post post);
    }

    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private List<Post> postList;

    public HomeAdapter(Context context, List<Post> postList, OnItemClickListener onItemClickListener){
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListener = onItemClickListener;
        this.postList = postList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType == POST_VIEW_TYPE) {
            View view = this.layoutInflater.inflate(R.layout.home_item, parent, false);
            return new HomeViewHolder(view);
        } else{
            View view = this.layoutInflater.inflate(R.layout.loading_item, parent, false);
            return new LoadingPostsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof  HomeViewHolder){
            ((HomeViewHolder) holder).bind(postList.get(position), onItemClickListener);
        } else if (holder instanceof  LoadingPostsViewHolder){
            ((LoadingPostsViewHolder) holder).progressBarLoadingNews.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        if(postList != null){
            return postList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(postList.get(position) == null) {
            return LOADING_VIEW_TYPE;
        } else {
            return POST_VIEW_TYPE;
        }
    }

    public void setData(List<Post> postList) {

        if(postList != null) {
            this.postList = postList;
            notifyDataSetChanged();
        }
    }

    public static class HomeViewHolder extends RecyclerView.ViewHolder {

        private final String TAG = "HomeViewHolder";

        TextView textViewEventTitle;
        //come attributi gli elementi di home_item

        public HomeViewHolder(View view){
            super(view);

            textViewEventTitle = view.findViewById(R.id.textViewEventTitle);
            //assegnare agli attributi il giusto elemento preso dal binding
        }

        public void bind(final Post post, final OnItemClickListener onItemClickListener){

            textViewEventTitle.setText("Titolo post: " + post.getNumber());
            //si impostano i valori degli elementi del layout dell'item

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(post);
                }
            });
        }
    }

    public static class LoadingPostsViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBarLoadingNews;

        public LoadingPostsViewHolder(View view){
            super(view);
            progressBarLoadingNews = view.findViewById(R.id.progressBarLoadingHome);
        }
    }*/
}
