package it.unimib.wattdo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import it.unimib.wattdo.R;
import it.unimib.wattdo.model.User;

/**
 * Adapter for the group members RecyclerView
 */
public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MembersViewHolder>{

    //the list of members that have to be shown
    private List<User> membersList;

    MembersAdapter(List<User> membersList) {
        this.membersList = membersList;
    }

    @NonNull
    @Override
    public MembersAdapter.MembersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.member_item, viewGroup, false);
        return new MembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MembersAdapter.MembersViewHolder viewHolder, final int position) {

        viewHolder.bind(membersList.get(position));
    }

    @Override
    public int getItemCount() {
        return membersList.size();
    }

    public static class MembersViewHolder extends RecyclerView.ViewHolder{

        private TextView memberNameTextView;
        private CircleImageView imageViewProfile;

        MembersViewHolder(View view) {
            super(view);

            memberNameTextView = view.findViewById(R.id.textViewMemberUsername);
            imageViewProfile = view.findViewById(R.id.imageViewProfile);
        }

        public void bind(final User user){

            memberNameTextView.setText(user.getUsername());

            //sets the image view source to the image retrieved by Picasso at the specified url
            if(user.getImageUrl() != null){
                Picasso.get().load(user.getImageUrl()).into(imageViewProfile);
            }
        }
    }
}
