package it.unimib.wattdo;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import it.unimib.wattdo.databinding.FragmentCreateProposalBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.CreateProposalViewModel;

/**
 * Fragment concerning the new proposal data entry
 */
public class CreateProposalFragment extends Fragment {

    private final String TAG = "CreateProposalFragment";

    private FragmentCreateProposalBinding binding;

    private String proposalName;
    private String description;
    private String location;
    private String time;
    private String imageUrl;

    private Event event;
    private CreateProposalViewModel viewModel;
    private FirebaseRepository firebaseRepository;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCreateProposalBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(requireActivity()).get(CreateProposalViewModel.class);
        firebaseRepository = new FirebaseRepository();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        event = viewModel.getEvent();

        Log.d(TAG, viewModel.getEvent().toString());
        Log.d(TAG, event.toString());

        binding.proposalTimeEditText.setOnClickListener(v -> setTime());

        binding.createProposalButton.setOnClickListener(v -> {
            time = binding.proposalTimeEditText.getText().toString();
            proposalName = binding.proposalNameEditText.getText().toString();
            description = binding.proposalDescriptionEditText.getText().toString();
            location = binding.locationEditText.getText().toString();
            if(checkData()) {
                if(checkDescription()) {

                    //if the inserted data pass the controls the proposal is created with the data and put in the database
                    imageUrl = binding.proposalUrlEditText.getText().toString();

                    Proposal proposal = new Proposal("", proposalName, description, time, imageUrl, event.getId());

                    //retrieve the proposal id
                    String proposalId = firebaseRepository.getPushId("events/" + event.getId() + "/proposals");
                    proposal.setId(proposalId);
                    //adds the proposal to the events proposals
                    firebaseRepository.setValue("events/" + event.getId() + "/proposals/" + proposalId, proposal);

                    Log.d(TAG, proposal.toString());

                    //when the proposal added to the database MainActivity is launched in the MyEventsFragment
                    Intent intent = new Intent(requireActivity(), MainActivity.class);
                    intent.putExtra("fragment", "events");
                    startActivity(intent);
                    requireActivity().finish();
                } else {
                    Snackbar.make(binding.getRoot(), R.string.description_lenght_snackbar, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        /*
        binding.selectLocationImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo scelta luogo tramite maps, api o altro
            }
        });
        */
    }

    /**
     * Check the inserted data
     */
    private boolean checkData() {
        return checkProposalName() && checkLocation() && checkTime();
    }

    /**
     * Check the inserted proposal name
     */
    private boolean checkProposalName() {
        if (proposalName.length() > 0 && proposalName.length() <= 15)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.proposal_name_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted location
     */
    private boolean checkLocation() {
        if (location.length() > 0)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.location_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted time
     */
    private boolean checkTime() {
        if (time.length() > 0)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.no_time_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the description length
     */
    private boolean checkDescription() {
        return description.length() <= 100;
    }

    /**
     * Check the time inserted and, if it passes the controls, set it as the corresponding proposal attribute
     */
    private void setTime() {

        TimePickerDialog.OnTimeSetListener timeSetListener = (view, hourOfDay, minute) -> {

            //format time
            String minuteString = minute + "";
            if (minuteString.length() == 1) {
                minuteString = "0" + minute;
            }

            String hourString = hourOfDay + "";
            if (hourString.length() == 1) {
                hourString = "0" + hourOfDay;
            }

            String timeString = hourString + ":" + minuteString;

            LocalTime eventStartTimeTmp = LocalTime.parse(event.getFromTime(), DateTimeFormat.forPattern("HH:mm"));
            LocalTime timeTmp = LocalTime.parse(timeString, DateTimeFormat.forPattern("HH:mm"));

            //if the inserted begin time is after the event begin time a message pops up, else the begin time is set as the proposal begin time
            if (eventStartTimeTmp.isAfter(timeTmp)) {
                Snackbar.make(binding.getRoot(), R.string.event_begin_after_proposal_begin_snackbar, Snackbar.LENGTH_SHORT).show();
            } else {
                binding.proposalTimeEditText.setText(timeString);
                time = timeString;
            }
        };

        //creates the dialog and shows it
        TimePickerDialog dialog = new TimePickerDialog(requireContext(), android.R.style.Theme_Material_Light_Dialog, timeSetListener, 0, 0, true);
        dialog.show();
    }
}
