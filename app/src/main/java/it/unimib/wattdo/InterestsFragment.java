package it.unimib.wattdo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.unimib.wattdo.databinding.FragmentInterestsBinding;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.AuthenticationViewModel;
import it.unimib.wattdo.viewmodels.DatabaseViewModel;
import it.unimib.wattdo.viewmodels.SettingViewModel;

/**
 * Fragment concerning the interests selection.
 * Is a listener for the interests buttons contained in it.
 */
public class InterestsFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "InterestsFragment";
    private FragmentInterestsBinding binding;

    private Map<String, Integer> interests;
    private FirebaseRepository firebaseRepository;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_interests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentInterestsBinding.bind(view);

        firebaseRepository = new FirebaseRepository();

        //retrieve from the intent the value that represent from where this fragment was called (settings or registration)
        final String caller = InterestsFragmentArgs.fromBundle(requireArguments()).getCaller();

        //retrieve the right view model and user from it depending on from where this fragment was called
        DatabaseViewModel viewModel;
        final User user;
        if(caller.equals("setting")){
            viewModel = new ViewModelProvider(requireActivity()).get(SettingViewModel.class);
            user = ((SettingViewModel) viewModel).getCurrentUser();
        } else {
            viewModel = new ViewModelProvider(requireActivity()).get(AuthenticationViewModel.class);
            user = ((AuthenticationViewModel) viewModel).getCurrentUser();
        }

        //sets the interests list to the user's or to an empty list if he user doesn't have any
        if(user.getInterests() == null) {
            interests = new HashMap<>();
        }
        else {
            interests = user.getInterests();
        }

        Button barButton = binding.barOutlinedButton;
        barButton.setOnClickListener(this);
        Button cinemaButton = binding.cinemaOutlinedButton;
        cinemaButton.setOnClickListener(this);
        Button gamesButton = binding.gamesOutlinedButton;
        gamesButton.setOnClickListener(this);
        Button restaurantOutlinedButton = binding.restaurantOutlinedButton;
        restaurantOutlinedButton.setOnClickListener(this);
        Button sportButton = binding.sportOutlinedButton;
        sportButton.setOnClickListener(this);
        Button theatreOutlinedButton = binding.theatreOutlinedButton;
        theatreOutlinedButton.setOnClickListener(this);

        //iterate the list if it was called from the settings
        Iterator it = interests.entrySet().iterator();
        if(caller.equals("setting")) {

            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                String interest = (String) entry.getKey();

                //change the background color of the buttons that represent interests already present in the user's
                switch (interest) {
                    case "bar":
                        binding.barOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case "cinema":
                        binding.cinemaOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case "games":
                        binding.gamesOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case "restaurant":
                        binding.restaurantOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case "sport":
                        binding.sportOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case "theatre":
                        binding.theatreOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    default:
                        break;
                }
            }
        }

        binding.doneButton.setOnClickListener(v -> {
            Log.d(TAG, "done clicked: " + interests.toString());

            //when the done button is clicked if the interests list is not empty
            if(!interests.isEmpty()){
                binding.doneButton.setClickable(false);

                Log.d(TAG, "interests not empty");

                //the interests list is set as the users interests
                user.setInterests(interests);

                //the user interests are updated on the database
                firebaseRepository.setValue("users/" + user.getEmailWithoutDot() + "/" + "interests", interests);

                //when the interests are set, if the caller is the RegistrationFragment the MainActivity is launched
                if(caller.equals("registration")) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    requireActivity().finish();
                } else {

                    //otherwise the SettingFragment goes back to the SettingFragment
                    NavController navController = Navigation.findNavController(requireView());
                    navController.navigate(InterestsFragmentDirections.backToSettingAction());
                }

                binding.doneButton.setClickable(true);
            }
            else {
                binding.doneButton.setClickable(true);

                //if the interests list is empty display a message
                Snackbar.make(v, R.string.select_interests_snackbar, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    //when an interest button is clicked
    @Override
    public void onClick(View v) {
        Log.d(TAG, "interest clicked");

        //if the corresponding interests is already present in the interests list it is removed and the button background color become transparent
        //otherwise the interest is added to the list and the button background color become the primary color
        switch (v.getId()) {
            case R.id.barOutlinedButton:
                if(interests.containsKey("bar")) {
                    binding.barOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                    interests.remove("bar");
                }
                else{
                    binding.barOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("bar", 0);
                }
                break;

            case R.id.cinemaOutlinedButton:
                if(interests.containsKey("cinema")) {
                    binding.cinemaOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                    interests.remove("cinema");
                }
                else{
                    binding.cinemaOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("cinema", 0);
                }
                break;

            case R.id.gamesOutlinedButton:
                if(interests.containsKey("games")) {
                binding.gamesOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                interests.remove("games");
            }
                else{
                    binding.gamesOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("games", 0);
                }
                break;

            case R.id.restaurantOutlinedButton:
                if(interests.containsKey("restaurant")) {
                    binding.restaurantOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                    interests.remove("restaurant");
                }
                else{
                    binding.restaurantOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("restaurant", 0);
                }
                break;

            case R.id.sportOutlinedButton:
                if(interests.containsKey("sport")) {
                    binding.sportOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                    interests.remove("sport");
                }
                else{
                    binding.sportOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("sport", 0);
                }
                break;

            case R.id.theatreOutlinedButton:
                if(interests.containsKey("theatre")) {
                    binding.theatreOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
                    interests.remove("theatre");
                }
                else{
                    binding.theatreOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    interests.put("theatre", 0);
                }
                break;

            default:
                break;
        }
    }
}
