package it.unimib.wattdo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import it.unimib.wattdo.adapters.GroupsAdapter;
import it.unimib.wattdo.databinding.FragmentGroupsBinding;
import it.unimib.wattdo.model.Group;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.CreateEventViewModel;
import it.unimib.wattdo.viewmodels.DatabaseViewModel;
import it.unimib.wattdo.viewmodels.MainViewModel;

/**
 * Fragment concerning the user groups displaying
 */
public class GroupsFragment extends Fragment {

    private final String TAG = "GroupsFragment";

    private FragmentGroupsBinding binding;
    private RecyclerView recyclerView;
    private List<Group> groupList;
    private User currentUser;
    private DatabaseViewModel viewModel;
    private String caller;

    private long counter = 0;
    private View viewTmp;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentGroupsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewTmp = view;

        //retrieve from the intent the value that represent from where this fragment was called (event or main)
        caller = InterestsFragmentArgs.fromBundle(requireArguments()).getCaller();

        //retrieve the right view model and user from it depending on from where this fragment was called
        if(caller.equals("event")){
            viewModel = new ViewModelProvider(requireActivity()).get(CreateEventViewModel.class);
            currentUser = ((CreateEventViewModel) viewModel).getCurrentUser();
        } else {
            viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
            currentUser = ((MainViewModel) viewModel).getCurrentUser().getValue();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //sets up the recycler view
        recyclerView = viewTmp.findViewById(R.id.groupsRecyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //reads the user groups list from the database
        String path = "users/" + currentUser.getEmailWithoutDot() + "/groups";
        final LiveData<DataSnapshot> usersLiveData = viewModel.getDataSnapshotPathLiveData(path);
        usersLiveData.observe(getViewLifecycleOwner(), dataSnapshot -> {

        groupList = new ArrayList<>();

        //adds a null element in first position in the groups list representing the create group button for the adapter
        groupList.add(null);

        if(dataSnapshot != null) {
            if (dataSnapshot.exists()) {

                final long groupsNumber = dataSnapshot.getChildrenCount();

                //iterates the groups
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    final String dsKey = ds.getKey();

                    if(dsKey != null) {

                        //reads the group from the database
                        String path1 = "groups/" + dsKey;
                        final LiveData<DataSnapshot> groupsLiveData = viewModel.getSingleDataSnapshotPathLiveData(path1);
                        groupsLiveData.observe(getViewLifecycleOwner(), dataSnapshot1 -> {

                            if(dataSnapshot1 != null) {

                                //adds the group to the groupList
                                Group group = dataSnapshot1.getValue(Group.class);
                                groupList.add(group);

                                counter ++;

                                //if it is the last group
                                if (counter == groupsNumber) {

                                    if(caller.equals("event")){

                                        //if this fragment was called in the event creation process put the groups list in the view model
                                        ((CreateEventViewModel) viewModel).setGroupList(groupList);

                                        //create and set the recycler view adapter
                                        recyclerView.setAdapter(new GroupsAdapter(currentUser, binding, viewModel, requireActivity(), caller));
                                    } else {

                                        //if this fragment was called in the main activity create a MutableLiveData containing the group
                                        //list and put it in the view model
                                        MutableLiveData<List<Group>> groupListMutableLiveData = new MutableLiveData<>(groupList);
                                        ((MainViewModel) viewModel).setGroupList(groupListMutableLiveData);

                                        //wait until the fragment is attached to the activity to create and set the recycler view adapter
                                        boolean adapterSet = false;
                                        while(!adapterSet) {
                                            if (isAdded()) {
                                                recyclerView.setAdapter(new GroupsAdapter(currentUser, binding, viewModel, requireActivity(), caller));
                                                adapterSet = true;
                                            }
                                        }
                                    }

                                    counter = 0;
                                }
                            }
                        });
                    }
                }
            } else {

                //if the user does not have any groups
                if(caller.equals("event")){

                    //if this fragment was called in the event creation process put the empty groups list in the view model
                    ((CreateEventViewModel) viewModel).setGroupList(groupList);

                    //create and set the recycler view adapter
                    recyclerView.setAdapter(new GroupsAdapter(currentUser, binding, viewModel, requireActivity(), caller));
                } else {

                    //if this fragment was called in the main activity create a MutableLiveData containing the group
                    //list and put it in the view model
                    MutableLiveData<List<Group>> groupListMutableLiveData = new MutableLiveData<>(groupList);
                    ((MainViewModel) viewModel).setGroupList(groupListMutableLiveData);

                    //create and set the recycler view adapter
                    recyclerView.setAdapter(new GroupsAdapter(currentUser, binding, viewModel, requireActivity(), caller));
                }
            }
        }
        });
    }
}
