package it.unimib.wattdo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;


import java.util.Calendar;
import java.util.Locale;

import it.unimib.wattdo.databinding.FragmentCreateEventBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.viewmodels.CreateEventViewModel;

/**
 * Fragment concerning the new event data entry
 */
public class CreateEventFragment extends Fragment {
    private final String TAG = "CreateEventFragment";

    private FragmentCreateEventBinding binding;
    private CreateEventViewModel viewModel;

    private String eventName;
    private String date;
    private String fromTime;
    //should be added in future
    //private String toTime;
    private String pollEndTime;
    private String description;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCreateEventBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(requireActivity()).get(CreateEventViewModel.class);

        binding.fromDateEditText.setOnClickListener(v -> setDate(LocalDate.now(), binding.fromDateEditText));

        binding.fromTimeEditText.setOnClickListener(v -> {
            if (date != null)
                setTime(binding.fromTimeEditText);
            else
                Snackbar.make(binding.getRoot(), R.string.no_date_snackbar, Snackbar.LENGTH_SHORT).show();
        });

        /*
        binding.toTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromTime != null)
                    setTime(binding.toTimeEditText);
            }
        });
         */

        binding.pollEndEditText.setOnClickListener(v -> {
            if (fromTime != null)
                setTime(binding.pollEndEditText);
            else
                Snackbar.make(binding.getRoot(), R.string.no_from_time_snackbar, Snackbar.LENGTH_SHORT).show();
        });

        binding.createEventNextButton.setOnClickListener(v -> {
            eventName = binding.eventNameEditText.getText().toString();
            description = binding.descriptionEditText.getText().toString();

            if (checkData()) {
                //if the inserted data pass the controls the event is created with the data and put in the view model
                Event newEvent = new Event("", eventName, description, date, fromTime, pollEndTime, null);
                Log.d(TAG, newEvent.toString());

                viewModel.setEvent(newEvent);

                //when the event is created the GroupFragment is shown specifying that it is called for the creation of an event
                NavController navController = Navigation.findNavController(requireView());
                navController.navigate(CreateEventFragmentDirections.chooseGroupAction("event"));

            }
        });
    }

    /**
     * Check the inserted date and, if it passes the controls, set it as the event date
     */
    private void setDate(final LocalDate currentDate2, final EditText target) {
        LocalDate currentDate = LocalDate.fromCalendarFields(Calendar.getInstance());
        int year = currentDate.getYear();
        int month = currentDate.getMonthOfYear() - 1;
        int day = currentDate.getDayOfMonth();
        Log.d(TAG, "current date: " + currentDate);


        //uncomment when the toTime is added
        //DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(requireContext());
        //String dateString = dateFormat.format(LocalDate.parse(dayOfMonth + "/" + month+ "/" + year, DateTimeFormat.forPattern("dd/MM/yyyy")));

        DatePickerDialog.OnDateSetListener dateSetListener = (view, year1, month1, dayOfMonth) -> {

            //uncomment when the toTime is added
            //DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(requireContext());
            //String dateString = dateFormat.format(LocalDate.parse(dayOfMonth + "/" + month+ "/" + year, DateTimeFormat.forPattern("dd/MM/yyyy")));
            String dateString;
            LocalDate fromDateTmp;

            //if (Locale.getDefault().equals(Locale.ITALY)) {
                dateString = dayOfMonth + "/" + (month1 + 1) + "/" + year1;
                Log.d(TAG, "date string: " + dateString);
                fromDateTmp = LocalDate.parse(dateString, DateTimeFormat.forPattern("dd/MM/yyyy"));
            //}
            //else {
            //    dateString = year1 + "-" + (month1 + 1) + "-" + dayOfMonth;
            //    fromDateTmp = LocalDate.parse(dateString);
            //}
            Log.d(TAG, "from date: " + fromDateTmp);

            //if the inserted date isn't in the past set it as the event date
            if (fromDateTmp.isAfter(currentDate) || fromDateTmp.isEqual(currentDate)) {
                Log.i(TAG, "onDateSet: target = fromDate");
                fromTime = null;
                binding.fromTimeEditText.setText(null);
                pollEndTime = null;
                binding.pollEndEditText.setText(null);
                target.setText(dateString);
                date = binding.fromDateEditText.getText().toString();
                Log.i(TAG, "onDateSet: fromDate = " + date);
            } else
                Snackbar.make(binding.getRoot(), R.string.valid_date_snackbar, Snackbar.LENGTH_SHORT).show();
        };

        DatePickerDialog dialog = new DatePickerDialog(requireContext(), android.R.style.Theme_Material_Light_Dialog, dateSetListener, year, month, day);
        dialog.show();
    }

    /**
     * Check the time inserted at the target and, if it passes the controls, set it as the corresponding event attribute
     */
    private void setTime(final EditText target) {
        //uncomment when the toTime is added
        /*
        if (target == binding.toTimeEditText) {
            //LocalTime fromTimeTmp = LocalTime.parse(fromTime, DateTimeFormat.forPattern("HH:mm"));
            /*LocalTime toTimeTmp = LocalTime.parse(timeString, DateTimeFormat.forPattern("HH:mm"));
            if (toTimeTmp.isAfter(fromTimeTmp) || toTimeTmp.isEqual(fromTimeTmp)) {
                target.setText(timeString);
                toTime = binding.toTimeEditText.getText().toString();
            }
            else
                Snackbar.make(binding.getRoot(), "\"To time\" is before \"From time\"", Snackbar.LENGTH_SHORT).show();

        }
        else
            binding.toTimeEditText.setText(null);
        toTime = null;
        binding.toTimeEditText.setText(null);
        */

        TimePickerDialog.OnTimeSetListener timeSetListener = (view, hourOfDay, minute) -> {

            //format time
            String minuteString = minute + "";
            if (minuteString.length() == 1) {
                minuteString = "0" + minute;
            }

            String hourString = hourOfDay + "";
            if (hourString.length() == 1) {
                hourString = "0" + hourOfDay;
            }

            String timeString = hourString + ":" + minuteString;

            //uncomment when the toTime is added
            /*
            if (target == binding.toTimeEditText) {
                //LocalTime fromTimeTmp = LocalTime.parse(fromTime, DateTimeFormat.forPattern("HH:mm"));
                /*LocalTime toTimeTmp = LocalTime.parse(timeString, DateTimeFormat.forPattern("HH:mm"));
                if (toTimeTmp.isAfter(fromTimeTmp) || toTimeTmp.isEqual(fromTimeTmp)) {
                    target.setText(timeString);
                    toTime = binding.toTimeEditText.getText().toString();
                }
                else
                    Snackbar.make(binding.getRoot(), "\"To time\" is before \"From time\"", Snackbar.LENGTH_SHORT).show();

            }
            else*/
            LocalDate dateObj = LocalDate.parse(date, DateTimeFormat.forPattern("dd/MM/yyyy"));
            if (target == binding.pollEndEditText) {
                LocalTime fromTimeTmp = LocalTime.parse(fromTime, DateTimeFormat.forPattern("HH:mm"));
                LocalTime endTimeTmp = LocalTime.parse(timeString, DateTimeFormat.forPattern("HH:mm"));

                // if the inserted date is equal to the current date, the time can not be before the current time
                if (dateObj.isEqual(LocalDate.now()) && endTimeTmp.isBefore(LocalTime.now())) {
                    Snackbar.make(binding.getRoot(), R.string.poll_end_before_current_time_snackbar, Snackbar.LENGTH_SHORT).show();
                }
                //if the inserted end time is after its begin time a message pops up, else the end time is set as the event end time
                else if (endTimeTmp.isAfter(fromTimeTmp)) {
                    Snackbar.make(binding.getRoot(), R.string.poll_end_after_from_time_snackbar, Snackbar.LENGTH_LONG).show();
                } else {
                    target.setText(timeString);
                    pollEndTime = binding.pollEndEditText.getText().toString();
                }
            }
            else {

                //uncomment when the toTime is added
                    /*
                    binding.toTimeEditText.setText(null);
                    toTime = null;
                    binding.toTimeEditText.setText(null);
                     */

                //set the inserted begin time as the event begin time
                pollEndTime = null;
                binding.pollEndEditText.setText(null);
                LocalTime time = LocalTime.parse(timeString, DateTimeFormat.forPattern("HH:mm"));
                if (dateObj.isEqual(LocalDate.now())) {
                    if (time.isBefore(LocalTime.now())) {
                        Snackbar.make(binding.getRoot(), R.string.from_time_before_current_time_snackbar, Snackbar.LENGTH_SHORT).show();
                    } else {
                        target.setText(timeString);
                        fromTime = binding.fromTimeEditText.getText().toString();
                    }
                } else {
                    target.setText(timeString);
                    fromTime = binding.fromTimeEditText.getText().toString();
                }
            }
        };

        //creates the dialog and shows it
        TimePickerDialog dialog = new TimePickerDialog(requireContext(), android.R.style.Theme_Material_Light_Dialog, timeSetListener, 0, 0, true);
        dialog.show();
    }

    /**
     * Check the inserted data
     */
    private boolean checkData() {
        //uncomment when the toTime is added
        //checkFromToTime();

        checkPollEndTime();
        return checkEventName() && checkDate() && checkFromTime() && checkDescription();
    }

    /**
     * Check the inserted date
     */
    private boolean checkDate() {
        if (date != null)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.no_date_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted event name
     */
    private boolean checkEventName() {
        if (eventName.length() > 0 && eventName.length() <= 10)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.event_name_length_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Check the inserted starting time
     */
    private boolean checkFromTime() {
        if (fromTime != null)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.no_time_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }


    //uncomment when the toTime is added
    /*
    private void checkFromToTime() {
        if (toTime == null)
            toTime = fromTime;
    }
    */

    /**
     * Check the poll end time. If it wasn't inserted it is set as the same as the event begin time
     */
    private void checkPollEndTime() {
        if (pollEndTime == null)
            pollEndTime = fromTime;
    }

    /**
     * Check the description length
     */
    private boolean checkDescription() {
        if (description.length() <= 100)
            return true;
        else {
            Snackbar.make(binding.getRoot(), R.string.description_lenght_snackbar, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }
}
