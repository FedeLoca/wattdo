package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;
import androidx.navigation.fragment.NavHostFragment;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.unimib.wattdo.databinding.ActivityResultBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.model.Proposal;
import it.unimib.wattdo.repositories.FirebaseRepository;
import it.unimib.wattdo.viewmodels.ResultViewModel;

/**
 * Activity that contains the fragments concerning the poll result showing process
 */
public class ResultActivity extends AppCompatActivity {

    private final String TAG = "ResultActivity";

    private ActivityResultBinding binding;
    private FirebaseRepository firebaseRepository;
    private ResultViewModel viewModel;
    private Map<Proposal, Integer> votesMap;
    private Event event;
    private Proposal proposalResult;

    private boolean noProposals = false;
    private long counter = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        firebaseRepository = new FirebaseRepository();

        viewModel = new ViewModelProvider(this).get(ResultViewModel.class);

        //retrieve the event from the intent and put it inside the view model
        Intent intent = getIntent();
        event = intent.getParcelableExtra("event");

        if(event != null) {
            Log.d(TAG, event.toString());
        }

        viewModel.setEvent(event);

        votesMap = new HashMap<>();

        if (event.getResult() != null) {

            //if the event result value is equal to the string "0" it means that this event has no proposal
            //so there isn't a result and the NoProposalFragment is shown
            if (event.getResult().equals("0")) {

                noProposals = true;
                createView();

            } else {

                //if the result is a proposal id the corresponding proposal is read from the database
                String path = "/events/" + event.getId() + "/proposals/" + event.getResult();
                final LiveData<DataSnapshot> groupNameLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
                groupNameLiveData.observe(this, dataSnapshot -> {
                    if (dataSnapshot != null) {
                        if (dataSnapshot.exists()) {
                            proposalResult = dataSnapshot.getValue(Proposal.class);
                            Log.d(TAG, "proposal get value");

                            //the result card is filled with the proposal data
                            if (proposalResult != null) {
                                viewModel.setProposalResult(proposalResult);
                                Log.d(TAG, proposalResult.toString());
                                createView();
                            }
                        }
                    }
                });
            }
        } else {

            //if the result wasn't already calculated the list of the event proposal is read from the database
            String path = "events/" + event.getId() + "/proposals";
            final LiveData<DataSnapshot> proposalsLiveData = viewModel.getSingleDataSnapshotPathLiveData(path);
            proposalsLiveData.observe(this, dataSnapshot -> {

                if (dataSnapshot != null) {
                    if (dataSnapshot.exists()) {
                        final long proposalsNumber = dataSnapshot.getChildrenCount();

                        //the proposals are iterated
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            final Proposal proposal = ds.getValue(Proposal.class);

                            if (proposal != null) {
                                if (proposal.getVotes() != null) {
                                    //if the proposal has votes, the number of votes is added in the votesMap with the proposal as key
                                    votesMap.put(proposal, proposal.getVotes().size());
                                } else {
                                    //if the proposal does not have votes, zero is added in the votesMap with the proposal as key
                                    votesMap.put(proposal, 0);
                                }
                            }

                            counter++;

                            //if the proposal is the last one
                            if (counter == proposalsNumber) {
                                Iterator it = votesMap.entrySet().iterator();

                                //the proposal with the highest number of votes is the winner
                                //if there are no votes the first proposal of the map wins
                                //if there are two or more proposals with the same votes, the first of them in the map wins
                                int max = 0;
                                int proposalsCounter = 0;
                                while (it.hasNext()) {
                                    Map.Entry entry = (Map.Entry) it.next();

                                    if (proposalsCounter == 0) {
                                        proposalResult = ((Proposal) entry.getKey());
                                        max = ((Integer) entry.getValue());
                                    }

                                    if (((Integer) entry.getValue()) > max) {
                                        proposalResult = ((Proposal) entry.getKey());
                                        max = ((Integer) entry.getValue());
                                    }

                                    proposalsCounter++;
                                }

                                if (proposalResult != null) {

                                    //the event result is added on the database
                                    firebaseRepository.setValue("events/" + event.getId() + "/result", proposalResult.getId());

                                    Log.d(TAG, "proposal created");

                                    //the result card is filled with the proposal data
                                    viewModel.setProposalResult(proposalResult);
                                    Log.d(TAG, proposalResult.toString());
                                    createView();
                                }
                            }
                        }
                    } else {

                        //if this event has no proposal the result value is set to zero and the NoProposalFragment is shown
                        firebaseRepository.setValue("events/" + event.getId() + "/result", "0");

                        noProposals = true;
                        createView();
                    }
                }
            });
        }
    }

    /**
     * Creates the activity view
     */
    public void createView() {

        if(binding == null) {

            binding = ActivityResultBinding.inflate(getLayoutInflater());
            final View view = binding.getRoot();

            //if the event has no proposals the NoProposalFragment is shown
            if(noProposals){

                NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.result_nav_host_fragment);  // Hostfragment
                NavGraph graph = null;
                if(navHostFragment != null){
                    NavInflater inflater = navHostFragment.getNavController().getNavInflater();
                    graph = inflater.inflate(R.navigation.result_nav_graph);
                }

                if(graph != null){

                    graph.setStartDestination(R.id.noProposalsFragment);
                }

                if(navHostFragment != null){
                    navHostFragment.getNavController().setGraph(graph);
                }
            }

            setContentView(view);
        }
    }
}
