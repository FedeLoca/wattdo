package it.unimib.wattdo.livedata;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * The DatabaseLiveData class is a LiveData that contains a DataSnapshot retrieved
 * from the Firebase Real-Time Database using a Query or a DatabaseReference
 */
public class DatabaseLiveData extends LiveData<DataSnapshot> {

    private static final String TAG = "DatabaseLiveData";

    private final Query query;
    private final MyValueEventListener listener = new MyValueEventListener();

    private boolean listenerAdded = false;

    /**
     * If the listener must read the DataSnapshot only one time this value should
     * be equal to "single", else it should be equal to "multiple"
     */
    private final String type;

    /**
     * Constructor for queries
     */
    public DatabaseLiveData(Query query, String type) {
        this.query = query;
        this.type = type;
    }

    /**
     * Constructor for database references
     */
    public DatabaseLiveData(DatabaseReference ref, String type) {
        this.query = ref;
        this.type = type;
        Log.d(TAG, ref.toString());
    }

    /**
     * Adds the listener to the Query or the DatabaseReference when the LifecycleOwner is active
     * and it wasn't already added.
     * If the type is "single" it adds it as a one time listener, else it adds it as a multiple
     * times listener
     */
    @Override
    protected void onActive() {
        Log.d(TAG, "onActive");

        if(!listenerAdded) {
            if (type.equals("single")) {
                query.addListenerForSingleValueEvent(listener);
            } else {
                query.addValueEventListener(listener);
            }
        }
        listenerAdded = true;
    }

    /**
     * Remove the listener to the Query or the DatabaseReference when the LifecycleOwner is inactive.
     *
     *     commented because we don't want the listeners to be added and the value to be read every time
     *     the application goes inactive and then active
     *     @Override
     *     protected void onInactive() {
     *         Log.d(TAG, "onInactive");
     *         query.removeEventListener(listener);
     *     }
     */


    /**
     * The Firebase Real-Time Database listener
     */
    private class MyValueEventListener implements ValueEventListener {

        /**
         * Sets the DatabaseLiveData value to the DataSnapshot retrieved from the
         * Firebase Real-Time Database
         */
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            setValue(dataSnapshot);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "Can't listen to query " + query, databaseError.toException());
        }
    }
}
