package it.unimib.wattdo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.unimib.wattdo.adapters.PlaceAdapter;
import it.unimib.wattdo.databinding.FragmentHomeBinding;
import it.unimib.wattdo.model.Place;
import it.unimib.wattdo.model.Resource;
import it.unimib.wattdo.model.User;
import it.unimib.wattdo.viewmodels.MainViewModel;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Fragment concerning the visualization of near places reflecting the user's interests
 */
public class HomeFragment extends Fragment {

    private final String TAG = "HomeFragment";

    private Location location;
    private double latitude = 0;
    private double longitude = 0;

    private Map<String, Integer> interests;

    private View view;

    private MainViewModel userViewModel;
    private User currentUser;

    private FragmentHomeBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater());

        //View Model Profile
        userViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        currentUser = userViewModel.getCurrentUser().getValue();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view = view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //if location permission is already granted
        if ((ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

            //view is created
            createUI();

        } else {

            binding.relativeLayoutLoading.setVisibility(View.GONE);
        }
    }

    /**
     * Creates the fragment User Interface
     */
    @SuppressLint("MissingPermission")
    private void createUI() {

        Log.d(TAG, "create ui");

        binding.relativeLayoutLoading.setVisibility(View.VISIBLE);

        LocationManager locationManager = (LocationManager) requireActivity().getSystemService(LOCATION_SERVICE);
        Criteria c = new Criteria();
        //if we pass false than
        //it will check first satellite location than Internet and than Sim Network
        assert locationManager != null;
        String provider = locationManager.getBestProvider(c, false);

        //location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(provider != null) {
            location = locationManager.getLastKnownLocation(provider);
        }

        //take latitude and longitude to make our request based on our location
        if (location != null) {
            Log.d(TAG, "location != null, Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } else {
            Log.d(TAG, "location == null ");
        }

        interests = new HashMap<>();

        Button barButton = binding.barOutlinedButton;
        Button cinemaButton = binding.cinemaOutlinedButton;
        Button gamesButton = binding.gamesOutlinedButton;
        Button restaurantButton = binding.restaurantOutlinedButton;
        Button sportButton = binding.sportOutlinedButton;
        Button theatreButton = binding.theatreOutlinedButton;

        //layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.homeRecyclerView.setLayoutManager(layoutManager);

        final Observer<Resource<List<Place>>> observer = placesResource -> {

            if(placesResource.getData() != null) {
                for (int i = 0; i < placesResource.getData().size(); i++) {
                    Log.d(TAG, "Info " + i + " " + placesResource.getTotalResults() + " " + placesResource.getStatusCode() + " " + placesResource.getStatusMessage()+ latitude + " "+ longitude);
                }
            } else {
                Log.d(TAG, "Info errore:" + placesResource.getTotalResults() + " " + placesResource.getStatusCode() + " " + placesResource.getStatusMessage());
            }

            PlaceAdapter placeAdapter = new PlaceAdapter(getActivity(), placesResource.getData(), place -> {

                Log.d(TAG, "Elemento premuto " + place.getTitle());

                HomeFragmentDirections.ShowPlaceDetailAction action = HomeFragmentDirections.showPlaceDetailAction(place);
                Navigation.findNavController(view).navigate(action);
            });

            binding.relativeLayoutLoading.setVisibility(View.GONE);
            binding.homeRecyclerView.setVisibility(View.VISIBLE);
            binding.homeRecyclerView.setClickable(true);
            binding.homeRecyclerView.setAdapter(placeAdapter);
        };

        //when the user clicks on an interest button the instruction that makes the request is called
        barButton.setOnClickListener(v          -> requestDataSend("bar", observer));

        cinemaButton.setOnClickListener(v       -> requestDataSend("cinema", observer));

        gamesButton.setOnClickListener(v        -> requestDataSend("games", observer));

        restaurantButton.setOnClickListener(v   -> requestDataSend("restaurant", observer));

        sportButton.setOnClickListener(v        -> requestDataSend("sport", observer));

        theatreButton.setOnClickListener(v       -> requestDataSend("theatre", observer));

        //check and display on UI current user interests
        userViewModel.getCurrentUser().observe(requireActivity(), changedUser -> {

            currentUser = changedUser;

            //first interest in the list will be selected thanks to the variable "count"
            int cont = 0;

            if(!currentUser.getInterests().containsKey("bar")) {
                barButton.setVisibility(View.GONE);
            }else{
                cont++;
                barButton.performClick();
            }

            if(!currentUser.getInterests().containsKey("cinema")){
                cinemaButton.setVisibility(View.GONE);
            }else{
                if(cont==0){
                    cont++;
                    cinemaButton.performClick();
                }
            }

            if(!currentUser.getInterests().containsKey("games")){
                gamesButton.setVisibility(View.GONE);
            }else{
                if(cont==0){
                    cont++;
                    gamesButton.performClick();
                }
            }

            if(!currentUser.getInterests().containsKey("restaurant")){
                restaurantButton.setVisibility(View.GONE);
            }else{
                if(cont==0){
                    cont++;
                    restaurantButton.performClick();
                }
            }

            if(!currentUser.getInterests().containsKey("sport")){
                sportButton.setVisibility(View.GONE);
            }else{
                if(cont==0){
                    cont++;
                    sportButton.performClick();
                }
            }

            if(!currentUser.getInterests().containsKey("theatre")){
                theatreButton.setVisibility(View.GONE);
            }else{
                if(cont==0){
                    //cont++;
                    theatreButton.performClick();
                }
            }
        });
    }


    /**
     * Select only one interest at a time and send the request for the selected interest
     */
    private void requestDataSend(String interest, Observer<Resource<List<Place>>> observer){
        Log.d(TAG, interest + " clicked: " + interests.toString());

        //the call will be made using the local name
        String interestLocalName = null;

        if(!interests.containsKey("bar") && interest.equals("bar")) {
            binding.barOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("bar", 0);
            interestLocalName = getResources().getString(R.string.bar);
        }else{
            binding.barOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("bar");
        }

        if(!interests.containsKey("cinema") && interest.equals("cinema")) {
            binding.cinemaOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("cinema", 0);
            interestLocalName = getResources().getString(R.string.cinema);
        }else{
            binding.cinemaOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("cinema");
        }

        if(!interests.containsKey("games") && interest.equals("games")) {
            binding.gamesOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("games", 0);
            interestLocalName = getResources().getString(R.string.games);
        }else{
            binding.gamesOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("games");
        }

        if(!interests.containsKey("restaurant") && interest.equals("restaurant")) {
            binding.restaurantOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("restaurant", 0);
            interestLocalName = getResources().getString(R.string.restaurant);
        }else{
            binding.restaurantOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("restaurant");
        }

        if(!interests.containsKey("sport") && interest.equals("sport")) {
            binding.sportOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("sport", 0);
            interestLocalName = getResources().getString(R.string.sport);
        }else{
            binding.sportOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("sport");
        }

        if(!interests.containsKey("theatre") && interest.equals("theatre")) {
            binding.theatreOutlinedButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            interests.put("theatre", 0);
            interestLocalName = getResources().getString(R.string.theatre);
        }else{
            binding.theatreOutlinedButton.setBackgroundColor(Color.TRANSPARENT);
            interests.remove("theatre");
        }

            LiveData<Resource<List<Place>>> liveData;
            binding.homeRecyclerView.getRecycledViewPool().clear();
            binding.homeRecyclerView.setClickable(false);
            binding.homeRecyclerView.setVisibility(View.GONE);

            binding.relativeLayoutLoading.setVisibility(View.VISIBLE);


            Log.d(TAG,"prova locale: " + interestLocalName );
            liveData = userViewModel.getPlacesResource(interestLocalName,Locale.getDefault().getLanguage(),Locale.getDefault().getCountry(), latitude, longitude);
            Log.d(TAG, " LiveData " + liveData);
            liveData.observe(getViewLifecycleOwner(), observer);
    }
}
