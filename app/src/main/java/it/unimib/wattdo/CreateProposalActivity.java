package it.unimib.wattdo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import it.unimib.wattdo.databinding.ActivityCreateProposalBinding;
import it.unimib.wattdo.model.Event;
import it.unimib.wattdo.viewmodels.CreateProposalViewModel;

/**
 * Activity that contains the fragments concerning the proposal creation process
 */
public class CreateProposalActivity extends AppCompatActivity {

    private final String TAG = "CreateProposalActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCreateProposalBinding binding = ActivityCreateProposalBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        CreateProposalViewModel viewModel = new ViewModelProvider(this).get(CreateProposalViewModel.class);

        //retrieve the event from the intent and put it inside the view model
        Intent intent = getIntent();
        Event event = intent.getParcelableExtra("event");

        if(event != null){
            Log.d(TAG, event.toString());
        }

        viewModel.setEvent(event);
    }
}
